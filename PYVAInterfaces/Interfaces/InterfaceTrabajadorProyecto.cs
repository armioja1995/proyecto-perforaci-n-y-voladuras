﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models; 

namespace PYVA.Interfaces.Interfaces
{
    public interface InterfaceTrabajadorProyecto
    {
        void AddTrabajadorProyecto(ICollection<TrabajadorProyecto> trabajadorProyecto);

        TrabajadorProyecto FindTrabajadorProyecto(int id);

        void UpdateTrabajadorProyecto(TrabajadorProyecto trabajadorProyecto);

        void DeleteTrabajadorProyecto(int id);
    }
}
