﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;

namespace PYVA.Interfaces.Interfaces
{
    public interface InterfaceEPP
    {
        List<EPP> AllEpp();

        List<EPP> ByQueryAll(string query);

        EPP FindEpp(int id);

        void AddEpp(EPP epp);

        void UpdateEpp(EPP epp);
        
        void DeleteEpp(int id);

        int NombreExiste(string nombre, int id);

    }
}
