﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models; 

namespace PYVA.Interfaces.Interfaces
{
    public interface InterfaceTrabajador
    {
        List<Trabajador> AllTrabajador();

        List<Trabajador> ByQueryAll(string query, Boolean? activo);

        void AddTrabajador(Trabajador trabajador);

        Trabajador FindTrabajador(int id);

        void UpdateTrabajador(Trabajador trabajador);

        void DeleteTrabajador(int id);

        int DniIgual(string code, int id);

        int BuscaEmailRepetido(string email, int id);
    }
}
