﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
namespace PYVA.Interfaces.Interfaces
{
    public interface InterfaceProyecto
    {
        List<Proyecto> AllProyecto();

        List<Proyecto> ByQueryAll(string query, DateTime? fecha1, DateTime? fecha2);

        void AddProyecto(Proyecto proyecto);

        Proyecto FindProyecto(int id);

        void UpdateProyecto(Proyecto proyecto);

        void DeleteProyecto(int id);
    }
}
