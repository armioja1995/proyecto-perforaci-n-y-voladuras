﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models;

namespace PYVA.Interfaces.Interfaces
{
    public interface InterfaceClasificacion
    {
        List<Clasificacion> AllClasificacion();

        List<Clasificacion> ByQueryAll(string query);

        Clasificacion FindClasificacion(int id);

        void AddClasificacionp(Clasificacion clasificacion);

        void UpdateClasificacion(Clasificacion clasificacion);

        void DeleteClasificacion(int id);
    }
}
