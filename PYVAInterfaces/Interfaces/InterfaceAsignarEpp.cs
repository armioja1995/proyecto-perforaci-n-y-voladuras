﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models;

namespace PYVA.Interfaces.Interfaces
{
    public interface InterfaceAsignarEpp
    {
        void AddAsignarEPP(ICollection<AsignarEPP> asignarEPP);

        AsignarEPP FindAsignarEPP(int id);

        void UpdateAsignarEPP(AsignarEPP asignarEPP);

        void DeleteAsignarEPP(int id);
    }
}
