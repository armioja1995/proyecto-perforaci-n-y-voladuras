﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models; 


namespace PYVA.Interfaces.Interfaces
{
    public interface InterfaceEmpContratista
     {
        List<EmpContratista> AllEmpContratista();

        List<EmpContratista> ByQueryAll(string query);

        void AddEmpContratista(EmpContratista empContratista);

        EmpContratista FindEmpContratista(int id);

        void UpdateEmpContratista(EmpContratista empContratista);

        void DeleteEmpContratista(int id);

        int BuscaRucRepetido(string ruc,int id);

        int BuscaTelefonoRepetido(string telefono, int id);
    }
}



