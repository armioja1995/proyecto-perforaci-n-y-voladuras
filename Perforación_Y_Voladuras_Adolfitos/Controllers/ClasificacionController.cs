﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PYVA.Interfaces.Interfaces;
using PYVA.Models;
using Validators.TrabajadorValidators;

namespace Perforación_Y_Voladuras_Adolfitos.Controllers
{
    public class ClasificacionController : Controller
    {
       private InterfaceClasificacion repository;
       private ClasificacionValidator validator;
       public ClasificacionController(InterfaceClasificacion repository, ClasificacionValidator validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        [HttpGet]
        public ViewResult Index(string query = "")
        {
            var datos = repository.ByQueryAll(query);
            return View("Inicio", datos);
        }
        [HttpGet]
        public ViewResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public ActionResult Create(Clasificacion clasificacion)
        {

            if (validator.Pass(clasificacion))
            {
                repository.AddClasificacionp(clasificacion);

                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                return RedirectToAction("Index");
            }

            return View("Create", clasificacion);
        }
        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repository.FindClasificacion(id);
            return View("Edit", data);
        }

        [HttpPost]
        public ActionResult Edit(Clasificacion clasificacion)
        {
            repository.UpdateClasificacion(clasificacion);
            TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.DeleteClasificacion(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }



        [HttpGet]
        public ViewResult Details(int id)
        {
            var data = repository.FindClasificacion(id);
            return View("Details", data);
        }

        [HttpPost]
        public ActionResult Details(Trabajador trabajador)
        {
            return RedirectToAction("Index");
        }
    }
}
