﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//////////////
using PYVA.Interfaces.Interfaces;
using PYVA.Models;
using Validators.TrabajadorValidators;


namespace Perforación_Y_Voladuras_Adolfitos.Controllers
{
    public class EppController : Controller 
    {
        private InterfaceEPP interfaceEPP;
        private InterfaceClasificacion interfaceClasificacion;
        private EPPValidator validator;

        public EppController(InterfaceEPP interfaceEPP, EPPValidator validator, InterfaceClasificacion interfaceClasificacion)
        {
            this.interfaceEPP = interfaceEPP;
            this.validator = validator;
            this.interfaceClasificacion = interfaceClasificacion;
        }

        [HttpGet]
        public ViewResult Index(string query = "")
        {
            var datos = interfaceEPP.ByQueryAll(query);
            return View("Inicio", datos);
        }
        [HttpGet]
        public ViewResult Create()
        {
            var clasificacion = interfaceClasificacion.AllClasificacion();
            ViewData["IdClasificacion"] = new SelectList(clasificacion, "Id", "Nombre_Clasificacion");
            return View("Create");
        }
        [HttpPost]
        public ActionResult Create(EPP epp)
        {
            validator.Execute(epp);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (ModelState.IsValid)
            {
                interfaceEPP.AddEpp(epp);
                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                return RedirectToAction("Index");
            }
            var clasificacion = interfaceClasificacion.AllClasificacion();
            ViewData["IdClasificacion"] = new SelectList(clasificacion, "Id", "Nombre_Clasificacion");
            return View("Create", epp);
        }
        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = interfaceEPP.FindEpp(id);

            var clasificacion = interfaceClasificacion.AllClasificacion();

            ViewData["IdClasificacion"] = new SelectList(clasificacion, "Id", "Nombre_Clasificacion", data.IdClasificacion);

            return View("Edit", data);
        }

        [HttpPost]
        public ActionResult Edit(EPP epp)
        {
            validator.Execute(epp);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (ModelState.IsValid)
            {
                interfaceEPP.UpdateEpp(epp);
                TempData["UpdateSuccess"] = "Se actualizo correctamente";
                return RedirectToAction("Index");
            }

            var data = interfaceEPP.FindEpp(epp.Id);
            var clasificacion = interfaceClasificacion.AllClasificacion();
            ViewData["IdClasificacion"] = new SelectList(clasificacion, "Id", "Nombre_Clasificacion", data.IdClasificacion);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            interfaceEPP.DeleteEpp(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ViewResult Details(int id)
        {
            var data = interfaceEPP.FindEpp(id);
            return View("Details", data);
        }

    }
}
