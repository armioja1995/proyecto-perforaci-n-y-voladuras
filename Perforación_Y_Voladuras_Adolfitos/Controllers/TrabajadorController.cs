﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//////////////
using PYVA.Interfaces.Interfaces;
using PYVA.Models;
using Validators.TrabajadorValidators;

namespace Perforación_Y_Voladuras_Adolfitos.Controllers
{
    public class TrabajadorController : Controller
    {
        private InterfaceTrabajador repository;
        private InterfaceCargo interfaceCargo;
        private TrabajadorValidator validator;
        private InterfaceEPP interfaceEpp;

        public TrabajadorController(InterfaceTrabajador repository,TrabajadorValidator validator, InterfaceCargo interfaceCargo, InterfaceEPP interfaceEpp)
        {
            this.repository = repository;
            this.interfaceCargo = interfaceCargo;
            this.validator = validator;
            this.interfaceEpp = interfaceEpp;
        }

        [HttpGet]
        public ViewResult Index(string query = "", Boolean? activo = null)
        {
            var datos = repository.ByQueryAll(query,activo);
            return View("Inicio", datos);
        }

        //////////////////////////////////////////7//////

        [HttpGet]
        public ViewResult Create()
        {
            var cargo = interfaceCargo.AllCargo();
            ViewData["IdCargo"] = new SelectList(cargo, "Id", "NombreCargo");

            return View("Create");
        }

        [HttpPost]
        public ActionResult Create(Trabajador trabajador)
        {
            validator.Execute(trabajador);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (ModelState.IsValid)
            {
                repository.AddTrabajador(trabajador);
                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                return RedirectToAction("Index");
            }
            var cargo = interfaceCargo.AllCargo();
            ViewData["IdCargo"] = new SelectList(cargo, "Id", "NombreCargo");
            return View("Create", trabajador);
        }

        /////////////////////////////////////////////////

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repository.FindTrabajador(id);

            var cargo = interfaceCargo.AllCargo();

            ViewData["IdCargo"] = new SelectList(cargo, "Id", "NombreCargo", data.IdCargo);
            
            return View("Edit", data);
        }

        [HttpPost]
        public ActionResult Edit(Trabajador trabajador)
        {
            validator.Execute(trabajador);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));
            if (ModelState.IsValid)
            {
                repository.UpdateTrabajador(trabajador);
                TempData["UpdateSuccess"] = "Se actualizo correctamente";
                return RedirectToAction("Index");
            }
            var data = repository.FindTrabajador(trabajador.Id);
            var cargo = interfaceCargo.AllCargo();
            ViewData["IdCargo"] = new SelectList(cargo, "Id", "NombreCargo", data.IdCargo);
            return View("Edit",data);
        }

        ///////////////////////////////////////////

        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.DeleteTrabajador(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }
        /////////////////////////////////////////////

        [HttpGet]
        public ViewResult Details(int id)
        {
            var data = repository.FindTrabajador(id);
            return View("Details", data);
        }

        ////////////////////////////////////////////////

        public ActionResult AsignarEpp(string operacion = null, string query = "")
        {

            var trabajadors = repository.ByQueryAll(query, true);
            ViewBag.trabajador = trabajadors;


            return View("AsignarEpp");
        }

        [HttpPost]
        public ActionResult AsignarEpp(Trabajador ntrabajador, string operacion = null, string query = "")
        {

            ////////////////////
            if (ntrabajador == null)
            {
                ntrabajador = new Trabajador();
            }

            if (operacion == null)
            {
                //ICollection<AsignarEPP> asignarEpp;
                //asignarEpp = ntrabajador.AsignarEPP;

                if (CrearAsignarEpp(ntrabajador))
                {
                    repository.UpdateTrabajador(ntrabajador);
                    TempData["UpdateSuccess"] = "Se Guardo Correctamente";
                    return RedirectToAction("Index");
                }
            }
            else if (operacion.StartsWith("agregar-detalle-"))
            {

                string indexStr = operacion.Replace("agregar-detalle-", "");
                int index = 0;
                int.TryParse(indexStr, out index);

                ViewBag.DataTrabajadorID = index;

                //ntrabajador = repository.FindTrabajador(index);
                //ntrabajador.Id = ntrabajador.AsignarEPP.First().IdEPP;/////////////////////////////////
                ntrabajador.Id = index;
                ntrabajador = repository.FindTrabajador(ntrabajador.Id);

                ntrabajador.AsignarEPP.Add(new AsignarEPP());

            }
            else if (operacion.StartsWith("eliminar-detalle-"))
            {
                EliminarDetallePorIndice(ntrabajador, operacion);
            }

            var trabajadors = repository.ByQueryAll(query, true);
            ViewBag.trabajador = trabajadors;

            var epps = interfaceEpp.AllEpp();
            ViewBag.epp = epps;



            return View("AsignarEpp", ntrabajador);
        }

        private bool CrearAsignarEpp(Trabajador ntrabajador)
        {
            //validator.Execute(ntrabajador);
            //validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (ModelState.IsValid)
            {
                return true;
            }

            return false;
        }


        private static void EliminarDetallePorIndice(Trabajador ntrabajador, string operacion)
        {
            string indexStr = operacion.Replace("eliminar-detalle-", "");
            int index = 0;

            if (int.TryParse(indexStr, out index) && index >= 0 && index < ntrabajador.AsignarEPP.Count)
            {
                var item = ntrabajador.AsignarEPP.ToArray()[index];
                ntrabajador.AsignarEPP.Remove(item);
            }
        }


    }
}





        //public ActionResult AsignarEpp(string operacion = null, string query = "")
        //{

        //    var trabajadors = repository.ByQueryAll(query, true);
        //    ViewBag.trabajador = trabajadors;


        //    return View("AsignarEpp");
        //}

        //[HttpPost]
        //public ActionResult AsignarEpp(Trabajador ntrabajador, string operacion = null, string query = "")
        //{
            
        //    ////////////////////
        //    //if (ntrabajador == null)
        //    //{
        //    //    ntrabajador = new Trabajador();
        //    //}

        //    if (operacion == null)
        //    {
        //        //ICollection<AsignarEPP> asignarEpp;
        //        //asignarEpp = ntrabajador.AsignarEPP;

        //        if (CrearAsignarEpp(ntrabajador))
        //        {
        //            repository.UpdateTrabajador(ntrabajador);
        //            TempData["UpdateSuccess"] = "Se Guardo Correctamente";
        //            return RedirectToAction("Index");
        //        }
        //    }
        //    else if (operacion.StartsWith("agregar-detalle-"))
        //    {

        //        string indexStr = operacion.Replace("agregar-detalle-", "");
        //        int index = 0;
        //        int.TryParse(indexStr, out index);

        //        ViewBag.DataTrabajadorID = index;

        //        //ntrabajador = repository.FindTrabajador(index);
        //        ntrabajador = new Trabajador();
        //        //ntrabajador.Id = ntrabajador.AsignarEPP.First().IdEPP;/////////////////////////////////
        //        ntrabajador.Id = index;
        //        ntrabajador = repository.FindTrabajador(ntrabajador.Id);
                
        //        ntrabajador.AsignarEPP.Add(new AsignarEPP());

        //    }
        //    else if (operacion.StartsWith("eliminar-detalle-"))
        //    {
        //        EliminarDetallePorIndice(ntrabajador, operacion);
        //    }

        //    var trabajadors = repository.ByQueryAll(query, true);
        //    ViewBag.trabajador = trabajadors;

        //    var epps = interfaceEpp.AllEpp();
        //    ViewBag.epp = epps;



        //    return View("AsignarEpp", ntrabajador);
        //}

        //private bool CrearAsignarEpp(Trabajador ntrabajador)
        //{
        //    //validator.Execute(ntrabajador);
        //    //validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

        //    if (ModelState.IsValid)
        //    {
        //        return true;
        //    }

        //    return false;
        //}


        //private static void EliminarDetallePorIndice(Trabajador ntrabajador, string operacion)
        //{
        //    string indexStr = operacion.Replace("eliminar-detalle-", "");
        //    int index = 0;

        //    if (int.TryParse(indexStr, out index) && index >= 0 && index < ntrabajador.AsignarEPP.Count)
        //    {
        //        var item = ntrabajador.AsignarEPP.ToArray()[index];
        //        ntrabajador.AsignarEPP.Remove(item);
        //    }
        //}