﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PYVA.Interfaces.Interfaces;
using PYVA.Models;
using Validators.ProyectoValidators;

namespace Perforación_Y_Voladuras_Adolfitos.Controllers
{
    public class ProyectoController : Controller
    {
        private InterfaceProyecto repository;
        private ProyectoValidator validator;

        private InterfaceEmpContratista interfaceEmpContratista;
        private InterfaceTipoProyecto interfaceTipoProyecto;
        private InterfaceTrabajador interfaceTrabajador;
        private InterfaceTrabajadorProyecto interfaceTrabajadorProyecto;

        public ProyectoController(InterfaceProyecto repository, ProyectoValidator validator, InterfaceEmpContratista interfaceEmpContratista, InterfaceTipoProyecto interfaceTipoProyecto, InterfaceTrabajador interfaceTrabajador, InterfaceTrabajadorProyecto interfaceTrabajadorProyecto)
        {
            this.repository = repository;
            this.validator = validator;

            this.interfaceEmpContratista = interfaceEmpContratista;
            this.interfaceTipoProyecto = interfaceTipoProyecto;
            this.interfaceTrabajador = interfaceTrabajador;
            this.interfaceTrabajadorProyecto = interfaceTrabajadorProyecto;
        }

        [HttpGet]
        public ViewResult Index(string query = "", DateTime? fecha1 = null, DateTime? fecha2 = null)
        {
            var datos = repository.ByQueryAll(query, fecha1, fecha2);
            return View("Inicio", datos);
        }

        //////////////////////////////////////////7//////


        public ActionResult Create()
        {
            var dataEmpContratista = interfaceEmpContratista.AllEmpContratista();
            ViewData["IdEmpContratista"] = new SelectList(dataEmpContratista, "Id", "NombreEmpresaC");

            var dataTipoOperacion = interfaceTipoProyecto.AllTipoOperacion();
            ViewData["IdTipoOperacion"] = new SelectList(dataTipoOperacion, "Id", "Descripcion");

            var trabajador = interfaceTrabajador.AllTrabajador();
            ViewBag.trabajador = trabajador;

            return View();
        }

        [HttpPost]
        public ActionResult Create(Proyecto proyecto, string operacion = null, string query = "")
        {
            ////////////////////
            if (proyecto == null)
            {
                proyecto = new Proyecto();
            }

            if (operacion == null)
            {
                if (CrearProyecto(proyecto))
                {
                    repository.AddProyecto(proyecto);
                    TempData["UpdateSuccess"] = "Se Guardo Correctamente";
                    return RedirectToAction("Index");
                }
            }
            else if (operacion.StartsWith("agregar-detalle-"))
            {

                string indexStr = operacion.Replace("agregar-detalle-", "");
                int index = 0;
                int.TryParse(indexStr, out index);

                ViewBag.DataTrabajadorID = index;
                //ViewBag.DataTrabajadorID = interfaceTrabajador.FindTrabajador(index).Id;

                proyecto.TrabajadorProyecto.Add(new TrabajadorProyecto());
               
            }
            else if (operacion.StartsWith("eliminar-detalle-"))
            {
                EliminarDetallePorIndice(proyecto, operacion);
            }

            var dataEmpContratista = interfaceEmpContratista.AllEmpContratista();
            ViewData["IdEmpContratista"] = new SelectList(dataEmpContratista, "Id", "NombreEmpresaC");

            var dataTipoOperacion = interfaceTipoProyecto.AllTipoOperacion();
            ViewData["IdTipoOperacion"] = new SelectList(dataTipoOperacion, "Id", "Descripcion");

            var trabajador = interfaceTrabajador.ByQueryAll(query,true);
            ViewBag.trabajador = trabajador;

            return View(proyecto);
        }


        private bool CrearProyecto(Proyecto proyecto)
        {
            validator.Execute(proyecto);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (ModelState.IsValid)
            {
                return true;
            }

            return false;
        }


        private static void EliminarDetallePorIndice(Proyecto proyecto, string operacion)
        {
            string indexStr = operacion.Replace("eliminar-detalle-", "");
            int index = 0;

            if (int.TryParse(indexStr, out index) && index >= 0 && index < proyecto.TrabajadorProyecto.Count)
            {
                var item = proyecto.TrabajadorProyecto.ToArray()[index];
                proyecto.TrabajadorProyecto.Remove(item);
            }
        }
        /////////////////////////////////////////////////

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repository.FindProyecto(id);
            var dataEmpContratista = interfaceEmpContratista.AllEmpContratista();
            ViewData["IdEmpContratista"] = new SelectList(dataEmpContratista, "Id", "NombreEmpresaC");
            var dataTipoOperacion = interfaceTipoProyecto.AllTipoOperacion();
            ViewData["IdTipoOperacion"] = new SelectList(dataTipoOperacion, "Id", "Descripcion");

            return View("Edit", data);
        }

        [HttpPost]
        public ActionResult Edit(Proyecto proyecto)
        {
            validator.Execute(proyecto);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));
            if (ModelState.IsValid)
            {
                repository.UpdateProyecto(proyecto);
                TempData["UpdateSuccess"] = "Se actualizo correctamente";
                return RedirectToAction("Index");
            }

            var data = repository.FindProyecto(proyecto.Id);
            var dataEmpContratista = interfaceEmpContratista.AllEmpContratista();
            ViewData["IdEmpContratista"] = new SelectList(dataEmpContratista, "Id", "NombreEmpresaC");
            var dataTipoOperacion = interfaceTipoProyecto.AllTipoOperacion();
            ViewData["IdTipoOperacion"] = new SelectList(dataTipoOperacion, "Id", "Descripcion");

            return View("Edit", data);
        }

        ///////////////////////////////////////////

        [HttpGet]
        public ActionResult Delete(int id)
        {
            
            repository.DeleteProyecto(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
            

        }
        /////////////////////////////////////////////

        [HttpGet]
        public ViewResult Details(int id)
        {
            var data = repository.FindProyecto(id);
            return View("Details", data);
        }

    }
}
