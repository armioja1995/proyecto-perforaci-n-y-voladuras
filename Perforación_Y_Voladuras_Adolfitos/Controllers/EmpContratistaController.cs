﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//////////////
using PYVA.Interfaces.Interfaces;
using PYVA.Models;
using Validators.EmpContratistaValidators;

namespace Perforación_Y_Voladuras_Adolfitos.Controllers
{
    public class EmpContratistaController : Controller
    {
        private InterfaceEmpContratista repository;
        private EmpContratistaValidator validator;

        public EmpContratistaController(InterfaceEmpContratista repository, EmpContratistaValidator validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        [HttpGet]
        public ViewResult Index(string query = "")
        {
            var datos = repository.ByQueryAll(query);
            return View("Inicio", datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public ActionResult Create(EmpContratista empContratista)
        {
            validator.Execute(empContratista);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (ModelState.IsValid)
            {
                repository.AddEmpContratista(empContratista);
                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                return RedirectToAction("Index");
            }
           
            return View("Create", empContratista);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repository.FindEmpContratista(id);
            return View("Edit", data);
        }

        [HttpPost]
        public ActionResult Edit(EmpContratista empContratista)
        {
            validator.Execute(empContratista);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (ModelState.IsValid)
            {
                repository.UpdateEmpContratista(empContratista);
                TempData["UpdateSuccess"] = "Se actualizo correctamente";
                return RedirectToAction("Index");
            }

            var data = repository.FindEmpContratista(empContratista.Id);
            return View("Edit",data);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.DeleteEmpContratista(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ViewResult Details(int id)
        {
            var data = repository.FindEmpContratista(id);
            return View("Details", data);
        }


    }
}
