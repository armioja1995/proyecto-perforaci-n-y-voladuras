﻿using System.Web;
using System.Web.Mvc;

namespace Perforación_Y_Voladuras_Adolfitos
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}