﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PYVA.Models
{
    public class TipoOperacion
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
