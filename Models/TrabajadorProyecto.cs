﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PYVA.Models
{
    public class TrabajadorProyecto
    {
        
        public string NombrePuesto { get; set; }

        public Boolean Activo { get; set; }


        //Enlace
        public Trabajador Trabajador { get; set; }
        public int IdTrabajador { get; set; }

        public Proyecto Proyecto { get; set; }
        public int IdProyecto { get; set; }

    }
}
