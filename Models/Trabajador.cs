﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PYVA.Models
{
    public class Trabajador
    {
        public int Id { get; set; }
        public string Dni { get; set; }
        public string Nombres { get; set; }
        public string ApePat { get; set; }
        public string ApeMat { get; set; }
        public string Sexo { get; set; }
        public DateTime FechaNac { get; set; }
        public DateTime FechaIngreso { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public Boolean Activo { get; set; }
        public Boolean Curriculo { get; set; }
        public string Email { get; set; }

        //public byte[] Foto { get; set; }
        //public byte[] Adjunto { get; set; }


        public string NombreCompleto
        {
            get
            {
                return ApePat + " " + ApeMat + ", " + Nombres;
            }
        }

        //FK
        public Cargo Cargo { get; set; }
        public Int32 IdCargo { get; set; }


        ////////////////
        
        public ICollection<AsignarEPP> AsignarEPP { get; set; }

        public Trabajador()
        {
            this.AsignarEPP = new HashSet<AsignarEPP>();
        }
        
    }
}
