﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PYVA.Models
{
    public class EmpContratista
    {
        public int Id { get; set; }
        public string NombreEmpresaC { get; set; }
        public string Ruc { get; set; }
        public string Representante { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
    }
}
