﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PYVA.Models
{
    public class Proyecto
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }

        public string Ubicacion { get; set; }

        public DateTime FechaInicio { get; set; }
        public DateTime FechaEstimadaFin { get; set; }

        public Boolean Activo { get; set; }
        
        public Double Costo { get; set; }

        public EmpContratista EmpContratista { get; set; }
        public int IdEmpContratista { get; set; }

        public TipoOperacion TipoOperacion { get; set; }
        public int IdTipoOperacion { get; set; }


        public ICollection<TrabajadorProyecto> TrabajadorProyecto { get; set; }

        public Proyecto()
        {
            this.TrabajadorProyecto = new HashSet<TrabajadorProyecto>();
        }
    }
}
