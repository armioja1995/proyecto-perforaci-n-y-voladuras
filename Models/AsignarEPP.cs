﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PYVA.Models
{
    public class AsignarEPP
    {
        public DateTime FechaEntrega { get; set; }
        public DateTime FechaRenovacion { get; set; }
        public string Estado { get; set; } 

        ////////////////

        public Trabajador Trabajador { get; set; }
        public int IdTrabajador { get; set; }

        public EPP EPP { get; set; }
        public int IdEPP { get; set; }
    }
}
