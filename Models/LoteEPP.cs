﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PYVA.Models
{
    public class LoteEPP
    {
        public int Id { get; set; }
        public string Marca { get; set; }
        public DateTime FechaIngreso { get; set; }


        public EPP EPP { get; set; }
        public int IdEPP { get; set; }
    }
}
