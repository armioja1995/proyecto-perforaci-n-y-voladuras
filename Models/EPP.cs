﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PYVA.Models
{
    public class EPP
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Talla { get; set; }
        public int Stock { get; set; }
        //public byte[] Foto { get; set; }
        public Boolean Descontinuado { get; set; }
        public string Tipo { get; set; }

        public Clasificacion Clasificacion { get; set; }
        public int IdClasificacion { get; set; }
    }
}
