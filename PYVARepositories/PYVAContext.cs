﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using PYVA.Models;
using PYVA.Repositories.Mapping;

namespace PYVA.Repositories
{
    public class PYVAContext:DbContext
    {
        public PYVAContext()      
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<PYVAContext>());
        }

        public virtual IDbSet<Cargo> Cargos { get; set; }
        public virtual IDbSet<Proyecto> Proyectos { get; set; }
        public virtual IDbSet<TipoOperacion> TipoOperacions { get; set; }
        public virtual IDbSet<Trabajador> Trabajadors { get; set; }
        public DbSet<TrabajadorProyecto> TrabajadorProyectos { get; set; }

        public virtual DbSet<AsignarEPP> AsignarEPPs { get; set; }
        public virtual IDbSet<Clasificacion> Clasificacions { get; set; }
        public virtual IDbSet<EPP> EPPs { get; set; }
        public virtual IDbSet<LoteEPP> LoteEPPs { get; set; }
        public virtual IDbSet<EmpContratista> EmpContratistas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CargoMap());
            modelBuilder.Configurations.Add(new ProyectoMap());
            modelBuilder.Configurations.Add(new TipoOperacionMap());
            modelBuilder.Configurations.Add(new TrabajadorMap());
            modelBuilder.Configurations.Add(new TrabajadorProyectoMap());

            modelBuilder.Configurations.Add(new AsignarEPPMap());
            modelBuilder.Configurations.Add(new ClasificacionMap());
            modelBuilder.Configurations.Add(new EPPMap());
            modelBuilder.Configurations.Add(new LoteEPPMap());
            modelBuilder.Configurations.Add(new EmpContratistaMap());
        }
    }
}
