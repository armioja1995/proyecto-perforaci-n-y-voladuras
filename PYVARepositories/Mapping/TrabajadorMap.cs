﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace PYVA.Repositories.Mapping
{
    public class TrabajadorMap : EntityTypeConfiguration<Trabajador>
    {
        public TrabajadorMap()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.Dni)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.Nombres)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.ApePat)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.ApeMat)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.Direccion)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.Curriculo);

            this.Property(p => p.Email)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.Activo);

            this.Property(p => p.FechaIngreso);

            this.Property(p => p.Telefono);

            //this.Property(p => p.Foto);

            this.Property(p => p.Sexo)
                .HasMaxLength(1)
                .IsRequired();

            this.Property(p => p.FechaNac)
                .IsRequired();


            //nombre completo e ignora en la base de datos
            this.Ignore(t => t.NombreCompleto);

            //relacion  
            this.HasRequired(p => p.Cargo)
                .WithMany()
                .HasForeignKey(p => p.IdCargo)
                .WillCascadeOnDelete(false);

            this.ToTable("Trabajador");

        }
    }
}
