﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace PYVA.Repositories.Mapping
{
    public class ProyectoMap : EntityTypeConfiguration<Proyecto>
    {
        public ProyectoMap()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.Descripcion)
                .HasMaxLength(200)
                .IsRequired();
            this.Property(p => p.Ubicacion)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.FechaInicio)
                .IsRequired();

            this.Property(p => p.FechaEstimadaFin)
                .IsRequired();

            this.Property(p => p.Activo)
                .IsRequired();

            this.Property(p => p.Costo)
                .IsRequired();

            this.ToTable("Proyecto");

             //relacion  
            this.HasRequired(p => p.TipoOperacion)
                .WithMany()
                .HasForeignKey(p => p.IdTipoOperacion)
                .WillCascadeOnDelete(false);

            this.HasRequired(p => p.EmpContratista)
                .WithMany()
                .HasForeignKey(p => p.IdEmpContratista)
                .WillCascadeOnDelete(false);
        }
    }
}
