﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace PYVA.Repositories.Mapping
{
    public class TrabajadorProyectoMap : EntityTypeConfiguration<TrabajadorProyecto>
    {
        public TrabajadorProyectoMap()
        {

            this.HasKey(i => new { i.IdTrabajador, i.IdProyecto });//(clave primaria compuesta)
            Property(i => i.IdTrabajador)
            .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(i => i.IdProyecto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

  
            this.Property(p => p.NombrePuesto)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.Activo);

            this.ToTable("TrabajadorProyecto");

             //relacion  
            this.HasRequired(p => p.Trabajador)
                .WithMany()
                .HasForeignKey(p => p.IdTrabajador)
                .WillCascadeOnDelete(false);


            //relacion  
            this.HasRequired(p => p.Proyecto)
                .WithMany(i => i.TrabajadorProyecto)
                .HasForeignKey(p => p.IdProyecto)
                .WillCascadeOnDelete(true);
        }
    }
}
