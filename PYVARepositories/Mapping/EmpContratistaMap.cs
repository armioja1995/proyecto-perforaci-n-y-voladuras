﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace PYVA.Repositories.Mapping
{
    public class  EmpContratistaMap : EntityTypeConfiguration<EmpContratista>
    {
        public EmpContratistaMap()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            
            this.Property(p => p.NombreEmpresaC)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.Ruc)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.Representante)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.Direccion)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.Telefono)
                .HasMaxLength(200)
                .IsRequired();

            this.ToTable("EmpContrata");
        }
    }
}
