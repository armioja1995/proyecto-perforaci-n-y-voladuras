﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace PYVA.Repositories.Mapping
{
    public class EPPMap : EntityTypeConfiguration<EPP>
    {
        public EPPMap()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.Nombre)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.Talla)
                .HasMaxLength(50);

            this.Property(p => p.Stock)
                .IsRequired();

            this.Property(p => p.Descontinuado);

            this.Property(p => p.Tipo)
                .HasMaxLength(200)
                .IsRequired();
            //this.Property(p => p.Foto)
            //    .HasColumnType("image");


            this.ToTable("EPP");


            this.HasRequired(p => p.Clasificacion)
                .WithMany()
                .HasForeignKey(p => p.IdClasificacion)
                .WillCascadeOnDelete(false);
        }
    }
}
