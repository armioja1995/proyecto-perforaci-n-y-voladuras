﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace PYVA.Repositories.Mapping
{
    public class LoteEPPMap : EntityTypeConfiguration<LoteEPP>
    {
        public LoteEPPMap()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.Marca)
                .HasMaxLength(200)
                .IsRequired();

            this.Property(p => p.FechaIngreso)
                .IsRequired();

            this.ToTable("LoteEPP");

            this.HasRequired(p => p.EPP)
                .WithMany()
                .HasForeignKey(p => p.IdEPP)
                .WillCascadeOnDelete(false);
        }
    }
}
