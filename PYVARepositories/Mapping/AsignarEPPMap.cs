﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace PYVA.Repositories.Mapping
{
    public class AsignarEPPMap : EntityTypeConfiguration<AsignarEPP>
    {
        public AsignarEPPMap()
        {
            this.HasKey(i => new { i.IdTrabajador, i.IdEPP });//(clave primaria compuesta)
            Property(i => i.IdTrabajador)
            .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(i => i.IdEPP)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(p => p.FechaEntrega)
                .IsRequired();

            this.Property(p => p.FechaRenovacion)
                .IsRequired();

            this.Property(p => p.Estado)
                .IsRequired();

            this.ToTable("AsignarEPP");

            //relacion 
            this.HasRequired(p => p.EPP)
               .WithMany()
               .HasForeignKey(p => p.IdEPP)
               .WillCascadeOnDelete(false);

            this.HasRequired(p => p.Trabajador)
                .WithMany(i => i.AsignarEPP)
                .HasForeignKey(p => p.IdTrabajador)
                .WillCascadeOnDelete(false);



        }
    }
}
