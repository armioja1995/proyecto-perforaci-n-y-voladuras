﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using PYVA.Models;
using PYVA.Interfaces.Interfaces;

namespace PYVA.Repositories.Repositories
{
    public class ClasificacionRepository:InterfaceClasificacion
    {
        PYVAContext entities;
        public ClasificacionRepository(PYVAContext entities)
        {
            this.entities = entities;
        }

        public List<Clasificacion> AllClasificacion()
        {
            var result = from p in entities.Clasificacions select p;
            return result.ToList();
        }

        public List<Clasificacion> ByQueryAll(string query)
        {
            var dbQuery = (from p in entities.Clasificacions select p);

            if (!String.IsNullOrEmpty(query))
                dbQuery = dbQuery.Where(o => o.Nombre_Clasificacion.Contains(query));

            return dbQuery.ToList();  
        }

        public Clasificacion FindClasificacion(int id)
        {
            var result = from p in entities.Clasificacions where p.Id == id select p; ;
            return result.FirstOrDefault();
        }



        public void AddClasificacionp(Clasificacion clasificacion)
        {
            entities.Clasificacions.Add(clasificacion);
            entities.SaveChanges();
        }

        public void UpdateClasificacion(Clasificacion clasificacion)
        {
            entities.Entry(clasificacion).State = EntityState.Modified;
            entities.SaveChanges();
        }

        public void DeleteClasificacion(int id)
        {
            var existe = entities.Clasificacions.Find(id);

            if (existe != null)
            {
                entities.Clasificacions.Remove(existe);
                entities.SaveChanges(); ;
            }
        }
    }
}
