﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using PYVA.Models;
using PYVA.Interfaces.Interfaces;

namespace PYVA.Repositories.Repositories
{
    public class EPPRepository:InterfaceEPP
    {
         PYVAContext entities;
         public EPPRepository(PYVAContext entities)
        {
            this.entities = entities;
        }


        public List<EPP> AllEpp()
        {
            var result = from p in entities.EPPs select p;
            return result.ToList();
        }

        public List<EPP> ByQueryAll(string query)
        {
            var dbQuery = (from p in entities.EPPs.Include("Clasificacion") select p);

            if (!String.IsNullOrEmpty(query))
                dbQuery = dbQuery.Where(o => o.Nombre.Contains(query));

            return dbQuery.ToList();       
        }

        public void AddEpp(EPP epp)
        {
            entities.EPPs.Add(epp);
            entities.SaveChanges();
        }

        public EPP FindEpp(int id)
        {
            var result = from p in entities.EPPs.Include("Clasificacion") where p.Id == id select p; ;
            return result.FirstOrDefault();
        }

        public void UpdateEpp(EPP epp)
        {
            entities.Entry(epp).State = EntityState.Modified;
            entities.SaveChanges();
        }

        public void DeleteEpp(int id)
        {
            var existe = entities.EPPs.Find(id);

            if (existe != null)
            {
                entities.EPPs.Remove(existe);
                entities.SaveChanges(); ;
            }
        }

        public int NombreExiste(string nombre, int id)
        {
            return (from p in entities.EPPs where p.Nombre == nombre && p.Id != id select p).Count();

        }
    }
}
