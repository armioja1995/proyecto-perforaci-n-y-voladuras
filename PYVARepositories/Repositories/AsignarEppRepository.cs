﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using PYVA.Interfaces.Interfaces;
using PYVA.Models;

using System.Data.Entity;

namespace PYVA.Repositories.Repositories
{
    public class AsignarEppRepository : InterfaceAsignarEpp
    {
        PYVAContext entities;
        public AsignarEppRepository(PYVAContext entities)
        {
            this.entities = entities;
        }

        public void AddAsignarEPP(ICollection<AsignarEPP> asignarEPP)
        {
            entities.AsignarEPPs.AddRange(asignarEPP);
            entities.SaveChanges();
        }

        public AsignarEPP FindAsignarEPP(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateAsignarEPP(AsignarEPP asignarEPP)
        {
            entities.Entry(asignarEPP).State = EntityState.Modified;
            entities.SaveChanges();
        }

        public void DeleteAsignarEPP(int id)
        {
            var existe = entities.AsignarEPPs.Find(id);

            if (existe != null)
            {
                entities.AsignarEPPs.Remove(existe);
                entities.SaveChanges(); ;
            }
        }




    }
}
