﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using PYVA.Interfaces.Interfaces;
using PYVA.Models;

using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace PYVA.Repositories.Repositories
{
    public class ProyectoRepository : InterfaceProyecto
    {
        PYVAContext entities;
        public ProyectoRepository(PYVAContext entities)
        {
            this.entities = entities;
        }

        public List<Proyecto> AllProyecto()
        {
            var result = from p in entities.Proyectos select p;
            return result.ToList();
        }

        public List<Proyecto> ByQueryAll(string query, DateTime? fecha1, DateTime? fecha2)
        {
            var dbQuery = (from p in entities.Proyectos.Include("EmpContratista").Include("TipoOperacion") select p);

            if (!String.IsNullOrEmpty(query))
                dbQuery = dbQuery.Where(o => o.Descripcion.Contains(query) );

            if (fecha1 != null && fecha2!= null)
                dbQuery = dbQuery.Where(o => o.FechaInicio >= fecha1 &&  o.FechaInicio <= fecha2);

            return dbQuery.ToList();   
        }

        public void AddProyecto(Proyecto proyecto)
        {
            entities.Proyectos.Add(proyecto);
            entities.SaveChanges();

        }


        public Proyecto FindProyecto(int id)
        {
            var result = from p in entities.Proyectos.Include("EmpContratista").Include("TipoOperacion") where p.Id == id select p; ;
            return result.FirstOrDefault();
        }

        public void UpdateProyecto(Proyecto proyecto)
        {
            entities.Entry(proyecto).State = EntityState.Modified;
            entities.SaveChanges();
        }

        public void DeleteProyecto(int id)
        {
            var existe = entities.Proyectos.Find(id);

            if (existe != null)
            {
                entities.Proyectos.Remove(existe);
                entities.SaveChanges(); ;
            }
        }
    }
}