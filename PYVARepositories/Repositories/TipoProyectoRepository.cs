﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Interfaces.Interfaces;
using PYVA.Models;

using System.Data.Entity;

namespace PYVA.Repositories.Repositories
{
    public class TipoProyectoRepository :InterfaceTipoProyecto
    {
        PYVAContext entities;
        public TipoProyectoRepository(PYVAContext entities)
        {
            this.entities = entities;
        }

        public List<TipoOperacion> AllTipoOperacion()
        {
            var result = from p in entities.TipoOperacions select p;
            return result.ToList();
        }
    }
}
