﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using PYVA.Interfaces.Interfaces;
using PYVA.Models;

using System.Data.Entity;

namespace PYVA.Repositories.Repositories
{
    public class TrabajadorRepository : InterfaceTrabajador
    {
        PYVAContext entities;
        public TrabajadorRepository(PYVAContext entities)
        {
            this.entities = entities;
        }


        public List<Trabajador> AllTrabajador()
        {
            var result = from p in entities.Trabajadors select p;
            return result.ToList();
        }

        public List<Trabajador> ByQueryAll(string query, Boolean? activo)
        {
            var dbQuery = (from p in entities.Trabajadors.Include("Cargo") select p);

            if (!String.IsNullOrEmpty(query))
                dbQuery = dbQuery.Where(o => o.Nombres.Contains(query) || o.ApePat.Contains(query) || o.ApeMat.Contains(query) || o.Dni.Contains(query));

            if (activo != null)
                dbQuery = dbQuery.Where(o => o.Activo == activo);

            return dbQuery.ToList();
        }



        public void AddTrabajador(Trabajador trabajador)
        {
            entities.Trabajadors.Add(trabajador);
            entities.SaveChanges();
        }



        public Trabajador FindTrabajador(int id)
        {
            var result = from p in entities.Trabajadors.Include("Cargo") where p.Id == id select p; ;
            return result.FirstOrDefault();
        }


        public void UpdateTrabajador(Trabajador trabajador)
        {
            entities.Entry(trabajador).State = EntityState.Modified;
            entities.SaveChanges();
        }



        public void DeleteTrabajador(int id)
        {          
            var existe = entities.Trabajadors.Find(id);

            if (existe != null)
            {
                entities.Trabajadors.Remove(existe);
                entities.SaveChanges(); ;
            }
        }

        public int DniIgual(string code, int id)
        {
            return (from p in entities.Trabajadors where p.Dni == code && p.Id != id select p).Count();
        }

        public int BuscaEmailRepetido(string email, int id)
        {
            return (from p in entities.Trabajadors where p.Email == email && p.Id != id select p).Count();
        }
    }
}
