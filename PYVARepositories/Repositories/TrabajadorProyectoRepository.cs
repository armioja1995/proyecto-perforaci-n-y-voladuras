﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using PYVA.Interfaces.Interfaces;
using PYVA.Models;

using System.Data.Entity;
namespace PYVA.Repositories.Repositories
{
    public class TrabajadorProyectoRepository : InterfaceTrabajadorProyecto
    {
        


        PYVAContext entities;
        public TrabajadorProyectoRepository(PYVAContext entities)
        {
            this.entities = entities;
        }

        //public void AddTrabajadorProyecto(TrabajadorProyecto trabajadorProyecto)
        //{
        //    entities.TrabajadorProyectos.Add(trabajadorProyecto);
        //    entities.SaveChanges();
        //}
        public void AddTrabajadorProyecto(ICollection<TrabajadorProyecto> trabajadorProyecto)
        {
            entities.TrabajadorProyectos.AddRange(trabajadorProyecto);
            entities.SaveChanges();
        }



        public void UpdateTrabajadorProyecto(TrabajadorProyecto trabajadorProyecto)
        {
            entities.Entry(trabajadorProyecto).State = EntityState.Modified;
            entities.SaveChanges();
        }

        public void DeleteTrabajadorProyecto(int id1, int id2)
        {
            var existe = entities.TrabajadorProyectos.Find(id1, id2);

            if (existe != null)
            {
                entities.TrabajadorProyectos.Remove(existe);
                entities.SaveChanges(); ;
            }
        }



        public TrabajadorProyecto FindTrabajadorProyecto(int id)
        {
            throw new NotImplementedException();
        }

        public void DeleteTrabajadorProyecto(int id)
        {
            throw new NotImplementedException();
        }

        
    }
}
