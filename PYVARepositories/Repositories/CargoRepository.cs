﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Interfaces.Interfaces;
using PYVA.Models;

using System.Data.Entity;

namespace PYVA.Repositories.Repositories
{
    public class CargoRepository: InterfaceCargo
    {
        PYVAContext entities;
        public CargoRepository(PYVAContext entities)
        {
            this.entities = entities;
        }

        public List<Cargo> AllCargo()
        {
            var result = from p in entities.Cargos select p;
            return result.ToList();
        }

    }
}
