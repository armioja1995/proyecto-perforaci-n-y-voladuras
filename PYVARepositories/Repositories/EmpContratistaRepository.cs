﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using PYVA.Models;
using PYVA.Interfaces.Interfaces;

namespace PYVA.Repositories.Repositories
{
    public class EmpContratistaRepository : InterfaceEmpContratista
    {
        PYVAContext entities;
        public EmpContratistaRepository(PYVAContext entities)
        {
            this.entities = entities;
        }



        public List<EmpContratista> AllEmpContratista()
        {
            var result = from p in entities.EmpContratistas select p;
            return result.ToList();
        }

        public List<EmpContratista> ByQueryAll(string query)
        {
            var dbQuery = (from p in entities.EmpContratistas select p);

            if (!String.IsNullOrEmpty(query))
                dbQuery = dbQuery.Where(o => o.NombreEmpresaC.Contains(query) || o.Ruc.Contains(query) || o.Representante.Contains(query));

            return dbQuery.ToList();    
        }

        public void AddEmpContratista(EmpContratista empContratista)
        {
            entities.EmpContratistas.Add(empContratista);
            entities.SaveChanges();
        }

        public EmpContratista FindEmpContratista(int id)
        {
            var result = from p in entities.EmpContratistas where p.Id == id select p; ;
            return result.FirstOrDefault();
        }

        public void UpdateEmpContratista(EmpContratista empContratista)
        {
            entities.Entry(empContratista).State = EntityState.Modified;
            entities.SaveChanges();
        }

        public void DeleteEmpContratista(int id)
        {
            var existe = entities.EmpContratistas.Find(id);

            if (existe != null)
            {
                entities.EmpContratistas.Remove(existe);
                entities.SaveChanges(); 
            }
        }


        public int BuscaRucRepetido(string ruc, int id)
        {
            return (from p in entities.EmpContratistas where p.Ruc == ruc && p.Id != id select p).Count();
        }


        public int BuscaTelefonoRepetido(string telefono, int id)
        {
            return (from p in entities.EmpContratistas where p.Telefono == telefono && p.Id != id select p).Count();
        }

       
    }
}
