﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using PYVA.Repositories;

namespace GenerateDB
{
    public class Program
    {
        static void Main(string[] args)
        {
            var cargo01 = new Cargo()
            {
                
                NombreCargo = "Operario"
            };

            var cargo02 = new Cargo()
            {

                NombreCargo = "Supervisor"
            };
            var cargo03 = new Cargo()
            {

                NombreCargo = "Jefe de Area"
            };
            var cargo04 = new Cargo()
            {

                NombreCargo = "Operador de Maquinaria"
            };
            var cargo05 = new Cargo()
            {

                NombreCargo = "Tecnico de Perforacion Y Voladuras"
            };

            var Clasificacion1 = new Clasificacion()
            {

                Nombre_Clasificacion = "P. CRANEAL"
            }; var Clasificacion2 = new Clasificacion()
            {

                Nombre_Clasificacion = "P. OCULAR"
            }; var Clasificacion3 = new Clasificacion()
            {

                Nombre_Clasificacion = "P. FACIAL"
            }; var Clasificacion4 = new Clasificacion()
            {

                Nombre_Clasificacion = "P. AUDITIVA"
            }; var Clasificacion5 = new Clasificacion()
            {

                Nombre_Clasificacion = "P. RESPIRATORIA"
            }; var Clasificacion6 = new Clasificacion()
            {

                Nombre_Clasificacion = "P. PARA DEL TRONCO"
            }; var Clasificacion7 = new Clasificacion()
            {

                Nombre_Clasificacion = "P. DE EXTREMIDADES"
            }; var Clasificacion8 = new Clasificacion()
            {

                Nombre_Clasificacion = "P. CORPORAL TOTAL"
            };
            var Clasificacion9 = new Clasificacion()
            {

                Nombre_Clasificacion = "P. TRABAJOS ESPECIALES"
            };
            var epp01 = new EPP()
            {
                Nombre = "Casco Rojo",
                Talla = "S",
                Stock = 23,
                Descontinuado = false,
                Tipo = "A",
                IdClasificacion = 1 
            };
            var epp02 = new EPP()
            {
                Nombre = "Lentes Transparentes",
                Talla = "S",
                Stock = 12,
                Descontinuado = false,
                Tipo = "Protección de Particulas",
                IdClasificacion = 3
            };

            var tipoOperacion01 = new TipoOperacion()
            {
                Descripcion = "Perforacion"
            };

            var tipoOperacion02 = new TipoOperacion()
            {
                Descripcion = "Voladura"
            };


            var empContratista01 = new EmpContratista()
            {
                NombreEmpresaC = "Municipalidad de Cajamarca",
                Representante = "Manuel Becerra",
                Ruc = "11111111111",
                Telefono = "989877744",
                Direccion = "Av. Atahualpa s/n"
            };

            var empContratista02 = new EmpContratista()
            {
                NombreEmpresaC = "MickyVainilla S.A",
                Representante = "Micky Vainilla",
                Ruc = "66666666666",
                Telefono = "989877444",
                Direccion = "Av. Argentina #123"
            };
            var empContratista03 = new EmpContratista()
            {
                NombreEmpresaC = "Newmon",
                Representante = "Jose Perez",
                Ruc = "9283928193",
                Telefono = "382918273",
                Direccion = "Av. La Paz #1123"
            };
            var empContratista04 = new EmpContratista()
            {
                NombreEmpresaC = "Yanacocha",
                Representante = "Lili Vasquez",
                Ruc = "28390293822",
                Telefono = "876538902",
                Direccion = "Jr. Lima #78392"
            };

            var proyecto01 = new Proyecto()
            {
               Descripcion="Contratos Generles para Perforacion",
               Ubicacion="Av. Hoyos Rubio",
               FechaInicio= new DateTime(12/07/2015),
               FechaEstimadaFin= new DateTime(12/09/2015),
               Activo = true,
               Costo = 12332.3,
               IdEmpContratista = 1,
               IdTipoOperacion = 1
            };
            var proyecto02 = new Proyecto()
            {
                Descripcion = "Ampliacion carreteras internas Yanacocha",
                Ubicacion = "Yanacocha",
                FechaInicio = new DateTime(16 / 08 / 2015),
                FechaEstimadaFin = new DateTime(30 / 12 / 2015),
                Activo = true,
                Costo = 3456789.232,
                IdEmpContratista = 4,
                IdTipoOperacion = 2
            };
            var trabajador01 = new Trabajador()
            {
                Dni="37283928",
                Nombres="Arnold Mijail",
                ApePat="Odar",
                ApeMat="Jauregui",
                Sexo="M",
                FechaNac=new DateTime(19/02/1995),
                FechaIngreso = new DateTime(03/12/2015),
                Direccion="Urb. Los Docentes I-22",
                Telefono="293827382",
                Activo=true,
                Curriculo=true,
                Email="armioja@hotmail.com",
                IdCargo=1
            };
            var trabajador02 = new Trabajador()
            {
                Dni = "38293827",
                Nombres = "Cristofer",
                ApePat = "Bardales",
                ApeMat = "Vargas",
                Sexo = "M",
                FechaNac = new DateTime(06/06/1993),
                FechaIngreso = new DateTime(01/11/2015),
                Direccion = "Jr. San Salvador s/n",
                Telefono = "676567890",
                Activo = true,
                Curriculo = true,
                Email = "vargass@hotmail.com",
                IdCargo=2
            };
            

            var context = new PYVAContext();
            Console.WriteLine("creando Base de Datos");

            context.Cargos.Add(cargo01);
            context.Cargos.Add(cargo02);
            context.Cargos.Add(cargo03);
            context.Cargos.Add(cargo04);
            context.Cargos.Add(cargo05);

            context.Clasificacions.Add(Clasificacion1);
            context.Clasificacions.Add(Clasificacion2);
            context.Clasificacions.Add(Clasificacion3);
            context.Clasificacions.Add(Clasificacion4);
            context.Clasificacions.Add(Clasificacion5);
            context.Clasificacions.Add(Clasificacion6);
            context.Clasificacions.Add(Clasificacion7);
            context.Clasificacions.Add(Clasificacion8);
            context.Clasificacions.Add(Clasificacion9);

            context.EPPs.Add(epp01);
            context.EPPs.Add(epp02);

            context.TipoOperacions.Add(tipoOperacion01);
            context.TipoOperacions.Add(tipoOperacion02);

            context.EmpContratistas.Add(empContratista01);
            context.EmpContratistas.Add(empContratista02);
            context.EmpContratistas.Add(empContratista03);
            context.EmpContratistas.Add(empContratista04);

            //context.Proyectos.Add(proyecto01);
            //context.Proyectos.Add(proyecto02);

            //context.Trabajadors.Add(trabajador01);
            //context.Trabajadors.Add(trabajador02);
            context.SaveChanges();

            Console.WriteLine("Base de Datos Creada OK!!");
            Console.ReadLine();

        }
    }
}
