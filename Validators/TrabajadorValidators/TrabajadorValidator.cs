﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using PYVA.Interfaces.Interfaces;
using PYVA.Repositories;
using PYVA.Repositories.Repositories;
using System.Text.RegularExpressions;


namespace Validators.TrabajadorValidators
{
    public class TrabajadorValidator
    {

        public Dictionary<String, String> errors = new Dictionary<string, string>();
        public void Execute(Trabajador trabajador)
        {
            Pass(trabajador);
        }

        public virtual bool Pass(Trabajador trabajador)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");
            //Regex regLetras = new Regex("^[A-Za-z ]+$");
            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");
            Regex regEmail = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

            if (String.IsNullOrEmpty(trabajador.Dni))
                errors.Add("Dni", "Ingrese DNI");
            else
            {
                if (!regNumeros.IsMatch(trabajador.Dni))
                    errors.Add("Dni", "El DNI debe contener solo digitos");
                else
                {
                    if (trabajador.Dni.Length != 8)
                        errors.Add("Dni", "El DNI debe contar con 8 digitos");
                    else
                    {
                        if (!dniNoExiste(trabajador.Dni, trabajador.Id))
                            errors.Add("Dni", "El DNI Ya existe");
                    }
                }
            }
            if (String.IsNullOrEmpty(trabajador.Nombres))
                errors.Add("Nombres", "Ingrese Nombre");
            else
            {
                if (!regLetras.IsMatch(trabajador.Nombres))
                    errors.Add("Nombres", "El Nombre debe contener solo Letras");
                else
                {
                    if (trabajador.Nombres.Length >= 50)
                        errors.Add("Nombres", "El Nombre debe tener hasta 50 letras");
                }
            }
            if (String.IsNullOrEmpty(trabajador.ApePat))
                errors.Add("ApePat", "Ingrese Apellido Paterno");
            else
            {
                if (!regLetras.IsMatch(trabajador.ApePat))
                    errors.Add("ApePat", "El Apellido debe contener solo Letras");
                else
                {
                    if (trabajador.ApePat.Length >= 50)
                        errors.Add("ApePat", "El Apellido debe tener hasta 50 letras");
                }
            }

            if (String.IsNullOrEmpty(trabajador.ApeMat))
                errors.Add("ApeMat", "Ingrese Apellido Materno");
            else
            {
                if (!regLetras.IsMatch(trabajador.ApeMat))
                    errors.Add("ApeMat", "El Apellido debe contener solo Letras");
                else
                {
                    if (trabajador.ApeMat.Length >= 50)
                        errors.Add("ApeMat", "El Apellido debe tener hasta 50 letras");
                }
            }

            if (String.IsNullOrEmpty(trabajador.Sexo))
                errors.Add("Sexo", "Seleccione Sexo");


            if (String.IsNullOrEmpty(trabajador.FechaNac.ToString()))
                errors.Add("FechaNac", "Seleccione Fecha Nacimiento");
            else 
            {
                int añoPermitido = DateTime.Now.Year - 18;
                if (trabajador.FechaNac.Year > añoPermitido)
                    errors.Add("FechaNac", "El trabajador tiene que ser mayor de edad, antes del año:" + añoPermitido);
            }

            if (String.IsNullOrEmpty(trabajador.FechaIngreso.ToString()))
                errors.Add("FechaIngreso", "Seleccione Fecha Nacimiento");


            if (String.IsNullOrEmpty(trabajador.Direccion))
                errors.Add("Direccion", "Ingrese Direccion");
            else
            {
                if (trabajador.Direccion.Length >= 50)
                    errors.Add("Direccion", "La Direccion debe tener hasta 50 caracteres");
            }


            if (String.IsNullOrEmpty(trabajador.Telefono))
                errors.Add("Telefono", "Ingrese Telefono");
            else
            {
                if (!regNumeros.IsMatch(trabajador.Telefono))
                    errors.Add("Telefono", "El Telefono debe contener solo digitos");
                else
                {
                    if (trabajador.Telefono.Length != 9)
                        errors.Add("Telefono", "El Telefono debe contar con 9 digitos");
                }
            }

            if (String.IsNullOrEmpty(trabajador.Email))
                errors.Add("Email", "Ingrese Email");
            else
            {
                if (!regEmail.IsMatch(trabajador.Email))
                    errors.Add("Email", "Email no valido, ejemplo: aabbcc@ejemplo.com");
                else
                {
                    if (trabajador.Email.Length >= 30)
                        errors.Add("Email", "El Email debe tener hasta 30 Caracteres");
                    else
                    {
                        if (!BuscaEmailRepetido(trabajador.Email, trabajador.Id))
                            errors.Add("Email", "El Email Ya existe");
                    }
                }
            }

            return true;
        }


        private bool dniNoExiste(string dni, int id)
        {
            var repo = new TrabajadorRepository(new PYVAContext());
            if (repo.DniIgual(dni, id) > 0)
                return false;
            return true;
        }

        private bool BuscaEmailRepetido(string email, int id)
        {
            var repo = new TrabajadorRepository(new PYVAContext());
            if (repo.BuscaEmailRepetido(email, id) > 0)
                return false;
            return true;
        }
        
    }
}
