﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using PYVA.Interfaces.Interfaces;
using PYVA.Repositories;
using PYVA.Repositories.Repositories;
using System.Text.RegularExpressions;

namespace Validators.TrabajadorValidators
{
    public class EPPValidator
    {
        public Dictionary<String, String> errors = new Dictionary<string, string>();
        public void Execute(EPP epp)
        {
            Pass(epp);
        }

        public virtual bool Pass(EPP epp)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");
            //Regex regLetras = new Regex("^[A-Za-z ]+$");
            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");
            Regex regEmail = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

            if (String.IsNullOrEmpty(epp.Nombre))
                errors.Add("Nombre", "Ingrese Nombre");
            else
            {
                if (!regLetras.IsMatch(epp.Nombre))
                    errors.Add("Nombre", "El Nombre debe contener solo Letras");
                else
                {
                    if (epp.Nombre.Length >= 50)
                        errors.Add("Nombre", "El Nombre Nombre debe tener hasta 50 letras");
                    else
                    {
                        if (!NombreNoExiste(epp.Nombre, epp.Id))
                        {
                            errors.Add("Nombre", "El Nombre ya existe");
                        }
                    }
                }
            }
        
            if (String.IsNullOrEmpty(epp.Talla))
                errors.Add("Talla", "Seleccione Talla");

            if (epp.Stock <= 1)
                errors.Add("Stock", "El Stock debe ser mayor o igual a 1 ");

            if (String.IsNullOrEmpty(epp.Tipo))
            {
                errors.Add("Tipo", "Ingrese Tipo");
            }
            else if (epp.Tipo.Length >= 50)
            {
                errors.Add("Tipo", "El tipo debe tener hasta 50 letras");
            }
            return true;  

        }
        private bool NombreNoExiste(string nombre, int id)
        {
            var repo = new EPPRepository(new PYVAContext());
            if (repo.NombreExiste(nombre, id) > 0)
                return false;
            return true;
        }
    }
}
