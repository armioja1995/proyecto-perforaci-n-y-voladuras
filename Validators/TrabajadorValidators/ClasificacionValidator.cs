﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models;

namespace Validators.TrabajadorValidators
{
    public class ClasificacionValidator
    {
        public virtual bool Pass(Clasificacion clasificacion)
        {
            if (String.IsNullOrEmpty(clasificacion.Nombre_Clasificacion))
                return false;
            return true;
        }
    }
}
