﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models;
//references

using PYVA.Interfaces.Interfaces;
using PYVA.Repositories;
using PYVA.Repositories.Repositories;
using System.Text.RegularExpressions;



namespace Validators.EmpContratistaValidators
{
    public class EmpContratistaValidator
    {
        public Dictionary<String, String> errors = new Dictionary<string, string>();
        public void Execute(EmpContratista empContratista)
        {
            Pass(empContratista);
        }

        public virtual bool Pass(EmpContratista empContratista)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");
            //Regex regLetras = new Regex("^[A-Za-z ]+$");
            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");
            Regex regEmail = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

            if (String.IsNullOrEmpty(empContratista.NombreEmpresaC))
                errors.Add("NombreEmpresaC", "Ingrese el Nombre de la Empresa");
            else
            {
                if (!regLetras.IsMatch(empContratista.NombreEmpresaC))
                    errors.Add("NombreEmpresaC", "El Nombre de la Empresa debe contener solo Letras");
                else
                {
                    if (empContratista.NombreEmpresaC.Length >= 100)
                        errors.Add("NombreEmpresaC", "El nombre de la empresa debe tener maximo 100 caracteres");
                }
            }

            if (String.IsNullOrEmpty(empContratista.Ruc))
                errors.Add("Ruc", "Ingrese RUC");
            else
            {
                if (!regNumeros.IsMatch(empContratista.Ruc))
                    errors.Add("Ruc", "El RUC debe contener solo digitos");
                else
                {
                    if (empContratista.Ruc.Length != 11)
                        errors.Add("Ruc", "El RUC debe contar con 11 digitos");
                    else
                    {
                        if (!RucNoExiste(empContratista.Ruc, empContratista.Id))
                            errors.Add("Ruc", "El RUC Ya existe");
                    }
                }

               
            }

            if (String.IsNullOrEmpty(empContratista.Representante))
                errors.Add("Representante", "Ingrese el Nombre del Represante");
            else
            {
                if (!regLetras.IsMatch(empContratista.Representante))
                    errors.Add("Representante", "El Nombre del Representante debe contener solo Letras");
                else
                {
                    if (empContratista.Representante.Length >= 50)
                        errors.Add("Representante", "El nombre del Representante debe tener maximo 50 caracteres");
                }
            }

            if (String.IsNullOrEmpty(empContratista.Direccion))
                errors.Add("Direccion", "Ingrese la Direccion");
            else
            {
                if (!regLetras.IsMatch(empContratista.Direccion))
                    errors.Add("Direccion", "La direccion debe contener solo Letras");
                else
                {
                    if (empContratista.Direccion.Length >= 50)
                        errors.Add("Direccion", "La direccion debe tener maximo 50 caracteres");
                }
            }

            if (String.IsNullOrEmpty(empContratista.Telefono))
                errors.Add("Telefono", "Ingrese Telefono");
            else
            {
                if (!regNumeros.IsMatch(empContratista.Telefono))
                    errors.Add("Telefono", "El Telefono debe contener solo digitos");
                else
                {
                    if (empContratista.Telefono.Length != 9)
                        errors.Add("Telefono", "El Telefono debe contar con 9 digitos");
                    else
                    {
                        if (!TelefonoYaExiste(empContratista.Telefono, empContratista.Id))
                            errors.Add("Telefono", "El Telefono Ya existe");
                    }
                }


            }


            return true;
        }



        private bool RucNoExiste(string ruc, int id)
        {
            var repo = new EmpContratistaRepository(new PYVAContext());
            if (repo.BuscaRucRepetido(ruc, id) > 0)
                return false;
            return true;
        }

        private bool TelefonoYaExiste(string telefono, int id)
        {
            var repo = new EmpContratistaRepository(new PYVAContext());
            if (repo.BuscaTelefonoRepetido(telefono, id) > 0)
                return false;
            return true;
        }
    }
}
