﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using PYVA.Interfaces.Interfaces;
using PYVA.Repositories;
using PYVA.Repositories.Repositories;
using System.Text.RegularExpressions;

namespace Validators.ProyectoValidators
{
    public class ProyectoValidator
    {
        public Dictionary<String, String> errors = new Dictionary<string, string>();
        public void Execute(Proyecto proyecto)
        {
            Pass(proyecto);
        }

        public virtual bool Pass(Proyecto proyecto)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");
            //Regex regLetras = new Regex("^[A-Za-z ]+$");
            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");
            Regex regEmail = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");


            if (String.IsNullOrEmpty(proyecto.Descripcion))
                errors.Add("Descripcion", "Ingrese Descripcion del Proyecto");
            else
            {
                if (!regLetras.IsMatch(proyecto.Descripcion))
                    errors.Add("Descripcion", "La Descripcion debe contener solo Letras");
                else
                {
                    if (proyecto.Descripcion.Length >= 50)
                        errors.Add("Descripcion", "La Descripcion debe tener hasta 50 letras");
                }
            }

            if (String.IsNullOrEmpty(proyecto.Ubicacion))
                errors.Add("Ubicacion", "Ingrese Ubicacion");
            else
            {
                if (!regLetras.IsMatch(proyecto.Ubicacion))
                    errors.Add("Ubicacion", "La Ubicacion debe contener solo Letras");
                else
                {
                    if (proyecto.Ubicacion.Length >= 50)
                        errors.Add("Ubicacion", "La Ubicacion debe tener hasta 50 letras");
                }
            }

            if (String.IsNullOrEmpty(proyecto.FechaInicio.ToString()))
                errors.Add("FechaInicio", "Seleccione Fecha Fecha de Inicio");
            else
            {
                if (proyecto.FechaInicio.Date <= DateTime.Now.Date)
                    errors.Add("FechaInicio", "La fecha de inicio Igual o despues de la fecha actual");
            }

            if (String.IsNullOrEmpty(proyecto.FechaEstimadaFin.ToString()))
                errors.Add("FechaInicio", "Seleccione Fecha Estimada de Fin");
            else
            {
                if (proyecto.FechaEstimadaFin.Date <= proyecto.FechaInicio.Date || proyecto.FechaEstimadaFin.Date <= DateTime.Now.Date)
                    errors.Add("FechaEstimadaFin", "La Fecha Estimada de Fin debe ser despues de la fecha de inicio");
            }


            if (proyecto.Costo <= 0.0)
                errors.Add("Costo", "El Costo debe ser mayor a S/. 0,00");

            if (String.IsNullOrEmpty(proyecto.IdEmpContratista.ToString()))
                errors.Add("IdEmpContratista", "Seleccione Empresa Contratista");

            if (String.IsNullOrEmpty(proyecto.IdTipoOperacion.ToString()))
                errors.Add("IdTipoOperacion", "Seleccione Tipo de Operacion");


            if (proyecto.TrabajadorProyecto.Count > 1)
            {
                List<TrabajadorProyecto> lista = proyecto.TrabajadorProyecto.Cast<TrabajadorProyecto>().ToList();

                for (int x = 0; x < proyecto.TrabajadorProyecto.Count; x++)
                {
                    for (int y = 0; y < proyecto.TrabajadorProyecto.Count; y++)
                        if (x != y && lista[x].IdTrabajador == lista[y].IdTrabajador)

                            if (!errors.ContainsKey("TrabajadorProyecto[" + y + "].IdTrabajador"))
                            {
                                errors.Add("TrabajadorProyecto[" + y + "].IdTrabajador", "Trabajadores repetidos");

                            }

                        if (lista[x].NombrePuesto == null)
                            errors.Add("TrabajadorProyecto[" + x + "].NombrePuesto", " Ingrese Nombre del puesto");
                } 
            }

            return true;
        }
    }
}
