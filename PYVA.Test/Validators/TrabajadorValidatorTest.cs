﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using Moq;
using NUnit.Framework;
using System.Data.Entity;
using System.Web.Mvc;
using Validators.TrabajadorValidators;

namespace PYVA.Test.Validators
{
    [TestFixture]
    public class TrabajadorValidatorTest
    {
        //[Test]
        public void TrabajadorSinNombreDebeRetornarFalse()
        {
            var validator = new TrabajadorValidator();
            var trabajador = new Trabajador {};

            var result = validator.Pass(trabajador);

            Assert.False(result);
        }

        [Test]
        public void TrabajadorConNombreYCodigoDebeRetornarTrue()
        {
            var validator = new TrabajadorValidator();
            var trabajador = new Trabajador
            {
                Dni = "001",
                Nombres = "Cristofr"
            };

            var result = validator.Pass(trabajador);

            Assert.True(result);

        }
    }
}
