﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using PYVA.Repositories.Repositories;
using PYVA.Repositories;
using Moq;
using NUnit.Framework;
using System.Data.Entity;

namespace PYVA.Test.Repositories
{
    [TestFixture]
    public class CargoRepositoryTest
    {
        private static CargoRepository RepoCargo()
        {
            var data = new List<Cargo>
            {
                new Cargo { Id = 1, NombreCargo="Supervisor" },
                new Cargo { Id = 2, NombreCargo="Operario"  }
            }.AsQueryable();

            var dbMock = new Mock<IDbSet<Cargo>>();

            dbMock.Setup(o => o.Provider).Returns(data.Provider);
            dbMock.Setup(o => o.Expression).Returns(data.Expression);
            dbMock.Setup(o => o.ElementType).Returns(data.ElementType);
            dbMock.Setup(o => o.GetEnumerator()).Returns(data.GetEnumerator());

            var dbContextMock = new Mock<PYVAContext>();
            dbContextMock.Setup(x => x.Cargos)
                .Returns(dbMock.Object);

            var repo = new CargoRepository(dbContextMock.Object);
            return repo;
        }

        [Test]
        public void AllCargo_ReturnList()
        {
            var repo = RepoCargo();

            var result = repo.AllCargo();

            Assert.IsInstanceOf(typeof(List<Cargo>), result);
            Assert.AreEqual(2, result.Count());
        }

    }
}
