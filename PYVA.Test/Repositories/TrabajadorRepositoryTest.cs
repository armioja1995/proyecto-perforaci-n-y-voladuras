﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using PYVA.Repositories.Repositories;
using PYVA.Repositories;
using Moq;
using NUnit.Framework;
using System.Data.Entity;


namespace PYVA.Test.Repositories
{
    [TestFixture]
    public class TrabajadorRepositoryTest
    {
        private static TrabajadorRepository RepoTrabajador()
        {
            var data = new List<Trabajador>
            {
                new Trabajador { Id = 1, Nombres = "Cristofer", ApePat = "Bardales", ApeMat="Vargas", Sexo="M", Activo=true , Curriculo=true , Direccion="jr Incayupanqui 489" , Dni="70272451" , Email="jonyoc6@outlook.com", IdCargo=1 , Telefono="989631679" , FechaNac = DateTime.Parse("06-06-1993") , FechaIngreso=DateTime.Parse("06-06-2015") },
                new Trabajador{ Id = 2, Nombres = "Jonathans", ApePat = "Bardales", ApeMat="Vargas", Sexo="F", Activo=true , Curriculo=true , Direccion="jr Incayupanqui 489" , Dni="70272451" , Email="jonyoc6@outlook.com", IdCargo=1 , Telefono="989631679" , FechaNac = DateTime.Parse("06-06-1993") , FechaIngreso=DateTime.Parse("06-06-2015") }
            }.AsQueryable();

            var dbMock = new Mock<IDbSet<Trabajador>>();

            dbMock.Setup(o => o.Provider).Returns(data.Provider);
            dbMock.Setup(o => o.Expression).Returns(data.Expression);
            dbMock.Setup(o => o.ElementType).Returns(data.ElementType);
            dbMock.Setup(o => o.GetEnumerator()).Returns(data.GetEnumerator());

            var dbContextMock = new Mock<PYVAContext>();
            dbContextMock.Setup(x => x.Trabajadors)
                .Returns(dbMock.Object);

            var repo = new TrabajadorRepository(dbContextMock.Object);
            return repo;
        }



        [Test]
        public void AllTrabajador_ReturnList()
        {
            var repo = RepoTrabajador();

            var result = repo.AllTrabajador();

            Assert.IsInstanceOf(typeof(List<Trabajador>), result);
            Assert.AreEqual(2, result.Count());
        }



        [Test]
        public void FindTrabajador_ReturnList()
        {
            var repo = RepoTrabajador();

            var result = repo.FindTrabajador(1);


            Assert.IsInstanceOf(typeof(Trabajador), result);
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("Cristofer", result.Nombres);
            Assert.AreEqual("Bardales", result.ApePat);

        }


        [Test]
        public void ByQueryAll_TestIsOk()
        {
            var repo = RepoTrabajador();

            var result = repo.ByQueryAll("Ba", null);

            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void DniIgual_TestIsOk()
        {
            var repo = RepoTrabajador();

            var result = repo.DniIgual("70272451", 1);

            Assert.AreEqual(1, result);
        }

        [Test]
        public void Delete_TestIsOk()
        {
           
            var trabajador = new Trabajador();

            

        }
        //[Test]
        //public void TestUpdateTrabajador_SaveChanges()
        //{
        //    var trabajador = new Trabajador();

        //    var repository = new Mock<TrabajadorRepository>();
            
        //    repository.Verify(o => o.AddTrabajador(trabajador), Times.Once());
        //}


        //[Test]
        //public void Will_call_save_changes() 
        //{ 
        //    var mockContext = new Mock<DBContext>(); 
        //    var unitOfWork = new UnitOfWork(mockContext.Object); 
        //    unitOfWork.Commit(); 
        //    mockContext.Verify(x => x.SaveChanges()); 
        //} 


    }
}
