﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using PYVA.Repositories.Repositories;
using PYVA.Repositories;
using Moq;
using NUnit.Framework;
using System.Data.Entity;

namespace PYVA.Test.Repositories
{
    [TestFixture]
    public class TipoProyectoRepositoryTest
    {
        private static TipoProyectoRepository RepoTipoOperacion()
        {
            var data = new List<TipoOperacion>
            {
                new TipoOperacion { Id = 1, Descripcion="Perforacion"},
                new TipoOperacion { Id = 2, Descripcion="Voladura"}
            }.AsQueryable();

            var dbMock = new Mock<IDbSet<TipoOperacion>>();

            dbMock.Setup(o => o.Provider).Returns(data.Provider);
            dbMock.Setup(o => o.Expression).Returns(data.Expression);
            dbMock.Setup(o => o.ElementType).Returns(data.ElementType);
            dbMock.Setup(o => o.GetEnumerator()).Returns(data.GetEnumerator());

            var dbContextMock = new Mock<PYVAContext>();
            dbContextMock.Setup(x => x.TipoOperacions)
                .Returns(dbMock.Object);

            var repo = new TipoProyectoRepository(dbContextMock.Object);
            return repo;
        }

        [Test]
        public void AllTipoOperacion_ReturnList()
        {
            var repo = RepoTipoOperacion();

            var result = repo.AllTipoOperacion();

            Assert.IsInstanceOf(typeof(List<TipoOperacion>), result);
            Assert.AreEqual(2, result.Count());
        }
    }
}
