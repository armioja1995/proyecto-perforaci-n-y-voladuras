﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using PYVA.Repositories.Repositories;
using PYVA.Repositories;
using Moq;
using NUnit.Framework;
using System.Data.Entity;

namespace PYVA.Test.Repositories
{
    [TestFixture]
    public class EppRepositoryTest
    {
        private static EPPRepository RepoEpp()
        {
            var data = new List<EPP>
            {
                new EPP { Id = 1, Nombre="Casco1", IdClasificacion=1, Descontinuado=true, Stock=66 , Talla="S" , Tipo="A" },
                new EPP { Id = 2, Nombre="Casco2", IdClasificacion=1, Descontinuado=true, Stock=66 , Talla="S" , Tipo="A" }
            }.AsQueryable();

            var dbMock = new Mock<IDbSet<EPP>>();

            dbMock.Setup(o => o.Provider).Returns(data.Provider);
            dbMock.Setup(o => o.Expression).Returns(data.Expression);
            dbMock.Setup(o => o.ElementType).Returns(data.ElementType);
            dbMock.Setup(o => o.GetEnumerator()).Returns(data.GetEnumerator());

            var dbContextMock = new Mock<PYVAContext>();
            dbContextMock.Setup(x => x.EPPs)
                .Returns(dbMock.Object);

            var repo = new EPPRepository(dbContextMock.Object);
            return repo;
        }

        [Test]
        public void AllEpp_ReturnList()
        {
            var repo = RepoEpp();

            var result = repo.AllEpp();

            Assert.IsInstanceOf(typeof(List<EPP>), result);
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void FindEpp_ReturnList()
        {
            var repo = RepoEpp();

            var result = repo.FindEpp(1);


            Assert.IsInstanceOf(typeof(EPP), result);
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("Casco1", result.Nombre);
            Assert.AreEqual(66, result.Stock);

        }

        [Test]
        public void ByQueryAllEpp_TestIsOk()
        {
            var repo = RepoEpp();

            var result = repo.ByQueryAll("Ca");

            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void NombreExiste_TestIsOk()
        {
            var repo = RepoEpp();

            var result = repo.NombreExiste("Casco2", 1);

            Assert.AreEqual(1, result);
        }
    }
}
