﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using PYVA.Repositories.Repositories;
using PYVA.Repositories;
using Moq;
using NUnit.Framework;
using System.Data.Entity;

namespace PYVA.Test.Repositories
{
    [TestFixture]
    public class ProyectoRepositoryTest
    {
        private static ProyectoRepository RepoProyecto()
        {
            var data = new List<Proyecto>
            {
                new Proyecto { Id = 1, Descripcion="Proyecto1", FechaInicio=DateTime.Parse("06-06-2015"), FechaEstimadaFin=DateTime.Parse("06-06-2015"), Costo=22.9 , IdEmpContratista=1, IdTipoOperacion=1,  Ubicacion="Ubicacion1", Activo=true },
                new Proyecto { Id = 2, Descripcion="Proyecto2", FechaInicio=DateTime.Parse("06-06-2015"), FechaEstimadaFin=DateTime.Parse("06-06-2015"), Costo=22.9 , IdEmpContratista=1, IdTipoOperacion=1,  Ubicacion="Ubicacion1", Activo=true }
            }.AsQueryable();

            var dbMock = new Mock<IDbSet<Proyecto>>();

            dbMock.Setup(o => o.Provider).Returns(data.Provider);
            dbMock.Setup(o => o.Expression).Returns(data.Expression);
            dbMock.Setup(o => o.ElementType).Returns(data.ElementType);
            dbMock.Setup(o => o.GetEnumerator()).Returns(data.GetEnumerator());

            var dbContextMock = new Mock<PYVAContext>();
            dbContextMock.Setup(x => x.Proyectos)
                .Returns(dbMock.Object);

            var repo = new ProyectoRepository(dbContextMock.Object);
            return repo;
        }

        [Test]
        public void AllProyecto_ReturnList()
        {
            var repo = RepoProyecto();

            var result = repo.AllProyecto();

            Assert.IsInstanceOf(typeof(List<Proyecto>), result);
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void FindProyecto_ReturnList()
        {
            var repo = RepoProyecto();

            var result = repo.FindProyecto(1);


            Assert.IsInstanceOf(typeof(Proyecto), result);
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("Proyecto1", result.Descripcion);

        }

        [Test]
        public void ByQueryAllProyecto_TestIsOk()
        {
            var repo = RepoProyecto();

            var result = repo.ByQueryAll("Pr", null, null);

            Assert.AreEqual(2, result.Count());
        }

    }
}
