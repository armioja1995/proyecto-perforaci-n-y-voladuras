﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PYVA.Models;
using PYVA.Repositories.Repositories;
using PYVA.Repositories;
using Moq;
using NUnit.Framework;
using System.Data.Entity;

namespace PYVA.Test.Repositories
{
    [TestFixture]
    public class ClasificacionRepositoryTest
    {
        private static ClasificacionRepository RepoClasificacion()
        {
            var data = new List<Clasificacion>
            {
                new Clasificacion { Id = 1, Nombre_Clasificacion="Craneal" },
                new Clasificacion { Id = 2, Nombre_Clasificacion="Ocular"  }
            }.AsQueryable();

            var dbMock = new Mock<IDbSet<Clasificacion>>();

            dbMock.Setup(o => o.Provider).Returns(data.Provider);
            dbMock.Setup(o => o.Expression).Returns(data.Expression);
            dbMock.Setup(o => o.ElementType).Returns(data.ElementType);
            dbMock.Setup(o => o.GetEnumerator()).Returns(data.GetEnumerator());

            var dbContextMock = new Mock<PYVAContext>();
            dbContextMock.Setup(x => x.Clasificacions)
                .Returns(dbMock.Object);

            var repo = new ClasificacionRepository(dbContextMock.Object);
            return repo;
        }

        [Test]
        public void AllClasificacion_ReturnList()
        {
            var repo = RepoClasificacion();

            var result = repo.AllClasificacion();

            Assert.IsInstanceOf(typeof(List<Clasificacion>), result);
            Assert.AreEqual(2, result.Count());
        }


        [Test]
        public void FindEmpContratista_ReturnList()
        {
            var repo = RepoClasificacion();

            var result = repo.FindClasificacion(1);

            Assert.IsInstanceOf(typeof(Clasificacion), result);
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("Craneal", result.Nombre_Clasificacion);
        }

        [Test]
        public void ByQueryAllClasificacion_TestIsOk()
        {
            var repo = RepoClasificacion();

            var result = repo.ByQueryAll("Cra");

            Assert.AreEqual(1, result.Count());
        }
    }
}
