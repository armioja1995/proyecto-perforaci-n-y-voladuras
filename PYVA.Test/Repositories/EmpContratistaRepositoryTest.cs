﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models;
using PYVA.Repositories.Repositories;
using PYVA.Repositories;
using Moq;
using NUnit.Framework;
using System.Data.Entity;


namespace PYVA.Test.Repositories
{
    [TestFixture]
    public class EmpContratistaRepositoryTest
    {
        private static EmpContratistaRepository RepoEmpContratista()
        {
            var data = new List<EmpContratista>
            {
                new EmpContratista { Id = 1, NombreEmpresaC="Empresa1", Ruc="11111111111", Direccion="jr el inca 489" , Representante="Trebor Trebor", Telefono="989631679"  },
                new EmpContratista { Id = 2, NombreEmpresaC="Empresa2", Ruc="11111111112", Direccion="jr el inca 489" , Representante="Trebor Trebor", Telefono="989631679"  }
            }.AsQueryable();

            var dbMock = new Mock<IDbSet<EmpContratista>>();

            dbMock.Setup(o => o.Provider).Returns(data.Provider);
            dbMock.Setup(o => o.Expression).Returns(data.Expression);
            dbMock.Setup(o => o.ElementType).Returns(data.ElementType);
            dbMock.Setup(o => o.GetEnumerator()).Returns(data.GetEnumerator());

            var dbContextMock = new Mock<PYVAContext>();
            dbContextMock.Setup(x => x.EmpContratistas)
                .Returns(dbMock.Object);

            var repo = new EmpContratistaRepository(dbContextMock.Object);
            return repo;
        }

        [Test]
        public void AllEmpContratista_ReturnList()
        {
            var repo = RepoEmpContratista();

            var result = repo.AllEmpContratista();

            Assert.IsInstanceOf(typeof(List<EmpContratista>), result);
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void FindEmpContratista_ReturnList()
        {
            var repo = RepoEmpContratista();

            var result = repo.FindEmpContratista(1);

            Assert.IsInstanceOf(typeof(EmpContratista), result);
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("Empresa1", result.NombreEmpresaC);
            Assert.AreEqual("11111111111", result.Ruc);
        }

        [Test]
        public void ByQueryAllEmpContratista_TestIsOk()
        {
            var repo = RepoEmpContratista();

            var result = repo.ByQueryAll("Em");

            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void RucExiste_TestIsOk()
        {
            var repo = RepoEmpContratista();

            var result = repo.BuscaRucRepetido("11111111112", 1);

            Assert.AreEqual(1, result);
        }

        [Test]
        public void BuscaTelefonoRepetido_TestIsOk()
        {
            var repo = RepoEmpContratista();

            var result = repo.BuscaTelefonoRepetido("989631679", 1);

            Assert.AreEqual(1, result);
        }
    }
}
