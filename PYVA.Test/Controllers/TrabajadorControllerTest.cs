﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models;
using PYVA.Repositories;
using Moq;
using NUnit.Framework;
using System.Data.Entity;
using PYVA.Interfaces.Interfaces;
using System.Web.Mvc;
using Perforación_Y_Voladuras_Adolfitos.Controllers;
using Validators.TrabajadorValidators;

namespace PYVA.Test.Controllers
{
    [TestFixture]
    public class TrabajadorControllerTest
    {
        [Test]
        public void TestIndexReturnView()
        {
            var mock = new Mock<InterfaceTrabajador>();

            var query = "";
            bool? activo = null;

            mock.Setup(o => o.ByQueryAll(query, activo)).Returns(new List<Trabajador>());

            var controller = new TrabajadorController(mock.Object, null,null,null);

            var view = controller.Index(query, activo);

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Inicio", view.ViewName);
            Assert.IsInstanceOf(typeof(List<Trabajador>), view.Model);

        }

        
        [Test]
        public void TestIndexCallAllMethodFromRepository()//este metodo no utilizo en el index
        {
            var mock = new Mock<InterfaceTrabajador>();
            mock.Setup(o => o.AllTrabajador()).Returns(new List<Trabajador>());

            var controller = new TrabajadorController(mock.Object, null, null, null);

            controller.Index();

            mock.Verify(o => o.AllTrabajador(), Times.Exactly(0));
        }

        //----------------------------------------------------------------------------------------
        [Test]
        public void TestCreateReturnView()
        {
            var mockCargo = new Mock<InterfaceCargo>();
            mockCargo.Setup(o => o.AllCargo()).Returns(new List<Cargo>());

            var controller = new TrabajadorController(null, null, mockCargo.Object, null);

            var view = controller.Create();

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Create", view.ViewName);
        }

        [Test]
        public void TestPostCreateOKReturnRedirect()
        {
            var trabajador = new Trabajador();

            var repositoryMock = new Mock<InterfaceTrabajador>();

            repositoryMock.Setup(o => o.AddTrabajador(new Trabajador()));

            var validatorMock = new Mock<TrabajadorValidator>();

            validatorMock.Setup(o => o.Pass(trabajador)).Returns(true);

            var controller = new TrabajadorController(repositoryMock.Object, validatorMock.Object,null,null);

            var redirect = (RedirectToRouteResult)controller.Create(trabajador);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestPostCreateCallAddMethodFromRepository()
        {
            var trabajador = new Trabajador();

            var repositoryMock = new Mock<InterfaceTrabajador>();

            var validatorMock = new Mock<TrabajadorValidator>();

            validatorMock.Setup(o => o.Pass(trabajador)).Returns(true);

            repositoryMock.Setup(o => o.AddTrabajador(trabajador));

            var controller = new TrabajadorController(repositoryMock.Object, validatorMock.Object,null,null);

            var redirect = controller.Create(trabajador);

            repositoryMock.Verify(o => o.AddTrabajador(trabajador), Times.Once());

        }

        [Test]

        public void TestPostCreateReturnViewWithErrorsWhenValidationFail()
        {
            var trabajador = new Trabajador { };

            var repositoryMock = new Mock<InterfaceTrabajador>();
            
            var mockValidador = new Mock<TrabajadorValidator>();
            mockValidador.Setup(o => o.Pass(trabajador)).Returns(false);

            var mockCargo = new Mock<InterfaceCargo>();
            mockCargo.Setup(o => o.AllCargo()).Returns(new List<Cargo>());

            var controller = new TrabajadorController(repositoryMock.Object, mockValidador.Object, mockCargo.Object, null);

            var view = controller.Create(trabajador);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), view);
        }
        //----------------------------------------------------------------------------------------

        [Test]
        public void TestEditReturnView()
        {
            //int id = 1;
            var trabajador = new Trabajador();

            var mockCargo = new Mock<InterfaceCargo>();
            mockCargo.Setup(o => o.AllCargo()).Returns(new List<Cargo>());

            var repositoryMock = new Mock<InterfaceTrabajador>();
            repositoryMock.Setup(o => o.FindTrabajador(trabajador.Id)).Returns(new Trabajador());

            var controller = new TrabajadorController(repositoryMock.Object, null, mockCargo.Object, null);

            var view = controller.Edit(trabajador.Id);

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Edit", view.ViewName);
        }

        [Test]
        public void TestPostEditOKReturnRedirect()
        {
            var trabajador = new Trabajador();

            var repositoryMock = new Mock<InterfaceTrabajador>();

            repositoryMock.Setup(o => o.UpdateTrabajador(new Trabajador()));

            var validatorMock = new Mock<TrabajadorValidator>();

            validatorMock.Setup(o => o.Pass(trabajador)).Returns(true);

            var controller = new TrabajadorController(repositoryMock.Object, validatorMock.Object, null, null);

            var redirect = (RedirectToRouteResult)controller.Edit(trabajador);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestPostEdirCallUpdateMethodFromRepository()
        {
            var trabajador = new Trabajador();

            var repositoryMock = new Mock<InterfaceTrabajador>();

            var validatorMock = new Mock<TrabajadorValidator>();

            validatorMock.Setup(o => o.Pass(trabajador)).Returns(true);

            repositoryMock.Setup(o => o.UpdateTrabajador(trabajador));

            var controller = new TrabajadorController(repositoryMock.Object, validatorMock.Object, null, null);

            var redirect = controller.Edit(trabajador);

            repositoryMock.Verify(o => o.UpdateTrabajador(trabajador), Times.Once());

        }

        [Test]

        public void TestPostEditReturnViewWithErrorsWhenValidationFail()
        {
            var trabajador = new Trabajador { };

            var repositoryMock = new Mock<InterfaceTrabajador>();

            var mockValidador = new Mock<TrabajadorValidator>();
            mockValidador.Setup(o => o.Pass(trabajador)).Returns(false);

            var mockCargo = new Mock<InterfaceCargo>();
            mockCargo.Setup(o => o.AllCargo()).Returns(new List<Cargo>());

            var controller = new TrabajadorController(repositoryMock.Object, mockValidador.Object, mockCargo.Object, null);

            var view = controller.Edit(trabajador);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), view);
        }
        //----------------------------------------------------------------------------------------

        [Test]
        public void TestDeleteReturnRouteResult()
        {
            var trabajador = new Trabajador();

            var repositoryMock = new Mock<InterfaceTrabajador>();
            repositoryMock.Setup(o => o.FindTrabajador(trabajador.Id)).Returns(new Trabajador());

            var controller = new TrabajadorController(repositoryMock.Object, null, null, null);

            var view = controller.Delete(trabajador.Id);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), view);
        }

        [Test]
        public void TestDetailsReturnView()
        {
            var trabajador = new Trabajador();

            var repositoryMock = new Mock<InterfaceTrabajador>();
            repositoryMock.Setup(o => o.FindTrabajador(trabajador.Id)).Returns(new Trabajador());

            var controller = new TrabajadorController(repositoryMock.Object, null, null, null);

            var view = controller.Details(trabajador.Id);

            Assert.AreEqual("Details", view.ViewName);
        }
    }
}
