﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models;
using PYVA.Repositories;
using Moq;
using NUnit.Framework;
using System.Data.Entity;
using PYVA.Interfaces.Interfaces;
using System.Web.Mvc;
using Perforación_Y_Voladuras_Adolfitos.Controllers;
using Validators.ProyectoValidators;

namespace PYVA.Test.Controllers
{
    [TestFixture]
    public class ProyectoControllerTest
    {
        [Test]
        public void TestProyectoIndexReturnView()
        {
            var mock = new Mock<InterfaceProyecto>();

            var query = "";

            mock.Setup(o => o.ByQueryAll(query, null,null)).Returns(new List<Proyecto>());

            var controller = new ProyectoController(mock.Object, null, null, null,null,null);

            var view = controller.Index(query, null,null);

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Inicio", view.ViewName);
            Assert.IsInstanceOf(typeof(List<Proyecto>), view.Model);

        }

        [Test]
        public void TestProyectoIndexCallAllMethodFromRepository()//este metodo no utilizo en el index
        {
            var mock = new Mock<InterfaceProyecto>();
            mock.Setup(o => o.AllProyecto()).Returns(new List<Proyecto>());

            var controller = new ProyectoController(mock.Object, null, null, null, null, null);

            controller.Index();

            mock.Verify(o => o.AllProyecto(), Times.Exactly(0));
        }

        //----------------------------------------------------------------------------------------
        [Test]
        public void TestProyectoCreateReturnView()
        {
            var mockTrabajador = new Mock<InterfaceTrabajador>();
            mockTrabajador.Setup(o => o.AllTrabajador()).Returns(new List<Trabajador>());

            var mockEmpContratista = new Mock<InterfaceEmpContratista>();
            mockEmpContratista.Setup(o => o.AllEmpContratista()).Returns(new List<EmpContratista>());

            var mockTipoProyecto = new Mock<InterfaceTipoProyecto>();
            mockTipoProyecto.Setup(o => o.AllTipoOperacion()).Returns(new List<TipoOperacion>());

            var controller = new ProyectoController(null, null, mockEmpContratista.Object, mockTipoProyecto.Object, mockTrabajador.Object, null);

            var view = controller.Create();

            Assert.IsInstanceOf(typeof(ViewResult), view);
        }

        [Test]
        public void TestProyectoPostCreateOKReturnRedirect()
        {
            var proyecto = new Proyecto();

            var repositoryMock = new Mock<InterfaceProyecto>();

            repositoryMock.Setup(o => o.AddProyecto(new Proyecto()));

            var mockTrabajador = new Mock<InterfaceTrabajador>();
            mockTrabajador.Setup(o => o.AllTrabajador()).Returns(new List<Trabajador>());

            var mockEmpContratista = new Mock<InterfaceEmpContratista>();
            mockEmpContratista.Setup(o => o.AllEmpContratista()).Returns(new List<EmpContratista>());

            var mockTipoProyecto = new Mock<InterfaceTipoProyecto>();
            mockTipoProyecto.Setup(o => o.AllTipoOperacion()).Returns(new List<TipoOperacion>());

            var validatorMock = new Mock<ProyectoValidator>();

            validatorMock.Setup(o => o.Pass(proyecto)).Returns(true);

            var controller = new ProyectoController(repositoryMock.Object, validatorMock.Object, mockEmpContratista.Object, mockTipoProyecto.Object, mockTrabajador.Object, null);


            var redirect = (RedirectToRouteResult)controller.Create(proyecto);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestPostCreateCallAddMethodFromRepository()
        {
            var proyecto = new Proyecto();

            var repositoryMock = new Mock<InterfaceProyecto>();

            repositoryMock.Setup(o => o.AddProyecto(new Proyecto()));

            var validatorMock = new Mock<ProyectoValidator>();

            validatorMock.Setup(o => o.Pass(proyecto)).Returns(true);

            var controller = new ProyectoController(repositoryMock.Object, validatorMock.Object, null, null,null,null);

            var redirect = controller.Create(proyecto);

            repositoryMock.Verify(o => o.AddProyecto(proyecto), Times.Once());

        }

        [Test]

        public void TestProyectoPostCreateReturnViewWithErrorsWhenValidationFail()
        {
            var proyecto = new Proyecto();

            var repositoryMock = new Mock<InterfaceProyecto>();

            repositoryMock.Setup(o => o.AddProyecto(new Proyecto()));

            var validatorMock = new Mock<ProyectoValidator>();

            validatorMock.Setup(o => o.Pass(proyecto)).Returns(true);

            var controller = new ProyectoController(repositoryMock.Object, validatorMock.Object, null, null, null, null);

            var view = controller.Create(proyecto);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), view);
        }
        //----------------------------------------------------------------------------------------

        [Test]
        public void TestProyectoEditReturnView()
        {
            var proyecto = new Proyecto();

            var repositoryMock = new Mock<InterfaceProyecto>();

            repositoryMock.Setup(o => o.AddProyecto(new Proyecto()));

            var mockTrabajador = new Mock<InterfaceTrabajador>();
            mockTrabajador.Setup(o => o.AllTrabajador()).Returns(new List<Trabajador>());

            var mockEmpContratista = new Mock<InterfaceEmpContratista>();
            mockEmpContratista.Setup(o => o.AllEmpContratista()).Returns(new List<EmpContratista>());

            var mockTipoProyecto = new Mock<InterfaceTipoProyecto>();
            mockTipoProyecto.Setup(o => o.AllTipoOperacion()).Returns(new List<TipoOperacion>());


            var controller = new ProyectoController(repositoryMock.Object, null, mockEmpContratista.Object, mockTipoProyecto.Object, mockTrabajador.Object, null);

            var view = controller.Edit(proyecto.Id);

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Edit", view.ViewName);
        }

        [Test]
        public void TestProyectoPostEditOKReturnRedirect()
        {
            var proyecto = new Proyecto();

            var repositoryMock = new Mock<InterfaceProyecto>();

            repositoryMock.Setup(o => o.UpdateProyecto(new Proyecto()));

            var validatorMock = new Mock<ProyectoValidator>();

            validatorMock.Setup(o => o.Pass(proyecto)).Returns(true);

            var controller = new ProyectoController(repositoryMock.Object, validatorMock.Object, null, null, null, null);

            var redirect = (RedirectToRouteResult)controller.Edit(proyecto);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestProyectoPostEdirCallUpdateMethodFromRepository()
        {
            var proyecto = new Proyecto();

            var repositoryMock = new Mock<InterfaceProyecto>();

            repositoryMock.Setup(o => o.UpdateProyecto(new Proyecto()));

            var validatorMock = new Mock<ProyectoValidator>();

            validatorMock.Setup(o => o.Pass(proyecto)).Returns(true);

            var controller = new ProyectoController(repositoryMock.Object, validatorMock.Object, null, null, null, null);

            var redirect = controller.Edit(proyecto);

            repositoryMock.Verify(o => o.UpdateProyecto(proyecto), Times.Once());

        }

        [Test]

        public void TestProyectoPostEditReturnViewWithErrorsWhenValidationFail()
        {
            var proyecto = new Proyecto();

            var repositoryMock = new Mock<InterfaceProyecto>();

            repositoryMock.Setup(o => o.UpdateProyecto(new Proyecto()));

            var validatorMock = new Mock<ProyectoValidator>();

            validatorMock.Setup(o => o.Pass(proyecto)).Returns(false);

            var controller = new ProyectoController(repositoryMock.Object, validatorMock.Object, null, null, null, null);

            var view = controller.Edit(proyecto);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), view);
        }
        //----------------------------------------------------------------------------------------

        [Test]
        public void TestProyectoDeleteReturnRouteResult()
        {
            var proyecto = new Proyecto();

            var repositoryMock = new Mock<InterfaceProyecto>();

            repositoryMock.Setup(o => o.UpdateProyecto(new Proyecto()));

            var controller = new ProyectoController(repositoryMock.Object, null, null, null, null, null);

            var view = controller.Delete(proyecto.Id);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), view);
        }

        [Test]
        public void TestProyectoDetailsReturnView()
        {
            var proyecto = new Proyecto();

            var repositoryMock = new Mock<InterfaceProyecto>();

            repositoryMock.Setup(o => o.UpdateProyecto(new Proyecto()));

            var controller = new ProyectoController(repositoryMock.Object, null, null, null, null, null);

            var view = controller.Details(proyecto.Id);

            Assert.AreEqual("Details", view.ViewName);
        }
    }
}
