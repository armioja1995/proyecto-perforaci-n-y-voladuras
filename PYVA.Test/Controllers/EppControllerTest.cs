﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models;
using PYVA.Repositories;
using Moq;
using NUnit.Framework;
using System.Data.Entity;
using PYVA.Interfaces.Interfaces;
using System.Web.Mvc;
using Perforación_Y_Voladuras_Adolfitos.Controllers;
using Validators.TrabajadorValidators;

namespace PYVA.Test.Controllers
{
    [TestFixture]
    public class EppControllerTest
    {
        [Test]
        public void TestEppIndexReturnView()
        {
            var mock = new Mock<InterfaceEPP>();

            var query = "";

            mock.Setup(o => o.ByQueryAll(query)).Returns(new List<EPP>());

            var controller = new EppController(mock.Object, null, null);

            var view = controller.Index(query);

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Inicio", view.ViewName);
            Assert.IsInstanceOf(typeof(List<EPP>), view.Model);

        }

        [Test]
        public void TestEppIndexCallAllMethodFromRepository()//este metodo no utilizo en el index
        {
            var mock = new Mock<InterfaceEPP>();

            var query = "";

            mock.Setup(o => o.ByQueryAll(query)).Returns(new List<EPP>());

            var controller = new EppController(mock.Object, null, null);

            controller.Index();

            mock.Verify(o => o.AllEpp(), Times.Exactly(0));
        }

        //----------------------------------------------------------------------------------------
        [Test]
        public void TestEppCreateReturnView()
        {

            var mockEmpClasificacion = new Mock<InterfaceClasificacion>();
            mockEmpClasificacion.Setup(o => o.AllClasificacion()).Returns(new List<Clasificacion>());

            var controller = new EppController(null, null, mockEmpClasificacion.Object);

            var view = controller.Create();

            Assert.IsInstanceOf(typeof(ViewResult), view);
        }

        [Test]
        public void TestEppPostCreateOKReturnRedirect()
        {
            var epp = new EPP();

            var repositoryMock = new Mock<InterfaceEPP>();

            repositoryMock.Setup(o => o.AddEpp(new EPP()));

            var validatorMock = new Mock<EPPValidator>();

            validatorMock.Setup(o => o.Pass(epp)).Returns(true);

            var controller = new EppController(repositoryMock.Object, validatorMock.Object, null);

            var redirect = (RedirectToRouteResult)controller.Create(epp);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestEppPostCreateCallAddMethodFromRepository()
        {


            var epp = new EPP();
            var repositoryMock = new Mock<InterfaceEPP>();

            var mockEmpClasificacion = new Mock<InterfaceClasificacion>();
            mockEmpClasificacion.Setup(o => o.AllClasificacion()).Returns(new List<Clasificacion>());

            var validatorMock = new Mock<EPPValidator>();

            validatorMock.Setup(o => o.Pass(epp)).Returns(true);

            var controller = new EppController(repositoryMock.Object, validatorMock.Object, mockEmpClasificacion.Object);

            var redirect = controller.Create(epp);

            repositoryMock.Verify(o => o.AddEpp(epp), Times.Once());

        }

        [Test]

        public void TestEppPostCreateReturnViewWithErrorsWhenValidationFail()
        {
            var epp = new EPP();
            var repositoryMock = new Mock<InterfaceEPP>();

            var validatorMock = new Mock<EPPValidator>();

            validatorMock.Setup(o => o.Pass(epp)).Returns(true);

            var controller = new EppController(repositoryMock.Object, validatorMock.Object, null);

            var view = controller.Create(epp);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), view);
        }
        //----------------------------------------------------------------------------------------

        [Test]
        public void TestEppEditReturnView()
        {
            var epp = new EPP();

            var repositoryMock = new Mock<InterfaceEPP>();
            repositoryMock.Setup(o => o.FindEpp(epp.Id)).Returns(new EPP());

            var mockEmpClasificacion = new Mock<InterfaceClasificacion>();
            mockEmpClasificacion.Setup(o => o.AllClasificacion()).Returns(new List<Clasificacion>());

            var validatorMock = new Mock<EPPValidator>();
            validatorMock.Setup(o => o.Pass(epp)).Returns(true);

            var controller = new EppController(repositoryMock.Object, validatorMock.Object, mockEmpClasificacion.Object);//
            var view = controller.Edit(epp.Id);

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Edit", view.ViewName);
        }

        [Test]
        public void TestEppPostEditOKReturnRedirect()
        {
            var epp = new EPP();
            var repositoryMock = new Mock<InterfaceEPP>();

            var validatorMock = new Mock<EPPValidator>();

            validatorMock.Setup(o => o.Pass(epp)).Returns(true);

            var controller = new EppController(repositoryMock.Object, validatorMock.Object, null);

            var redirect = (RedirectToRouteResult)controller.Edit(epp);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestEppPostEdirCallUpdateMethodFromRepository()
        {
            var epp = new EPP();
            var repositoryMock = new Mock<InterfaceEPP>();

            var validatorMock = new Mock<EPPValidator>();

            validatorMock.Setup(o => o.Pass(epp)).Returns(true);

            var controller = new EppController(repositoryMock.Object, validatorMock.Object, null);

            var redirect = controller.Edit(epp);

            repositoryMock.Verify(o => o.UpdateEpp(epp), Times.Once());

        }

        [Test]

        public void TestEppPostEditReturnViewWithErrorsWhenValidationFail()
        {
            var epp = new EPP();
            var repositoryMock = new Mock<InterfaceEPP>();

            var validatorMock = new Mock<EPPValidator>();

            validatorMock.Setup(o => o.Pass(epp)).Returns(true);

            var controller = new EppController(repositoryMock.Object, validatorMock.Object, null);

            var view = controller.Edit(epp);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), view);
        }
        //----------------------------------------------------------------------------------------

        [Test]
        public void TestEppDeleteReturnRouteResult()
        {
            var epp = new EPP();
            var repositoryMock = new Mock<InterfaceEPP>();

            var controller = new EppController(repositoryMock.Object, null, null);

            var view = controller.Delete(epp.Id);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), view);
        }

        [Test]
        public void TestEppDetailsReturnView()
        {
            var epp = new EPP();
            var repositoryMock = new Mock<InterfaceEPP>();

            var controller = new EppController(repositoryMock.Object, null, null);

            var view = controller.Details(epp.Id);

            Assert.AreEqual("Details", view.ViewName);
        }
    }
}
