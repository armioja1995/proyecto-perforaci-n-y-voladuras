﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PYVA.Models;
using PYVA.Repositories;
using Moq;
using NUnit.Framework;
using System.Data.Entity;
using PYVA.Interfaces.Interfaces;
using System.Web.Mvc;
using Perforación_Y_Voladuras_Adolfitos.Controllers;
using Validators.EmpContratistaValidators;

namespace PYVA.Test.Controllers
{
    [TestFixture]
    public class EmpContratistaControllerTest
    {
        [Test]
        public void TestEmpContraIndexReturnView()
        {
            var mock = new Mock<InterfaceEmpContratista>();

            var query = "";

            mock.Setup(o => o.ByQueryAll(query)).Returns(new List<EmpContratista>());

            var controller = new EmpContratistaController(mock.Object, null);

            var view = controller.Index(query);

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Inicio", view.ViewName);
            Assert.IsInstanceOf(typeof(List<EmpContratista>), view.Model);
        }

        [Test]
        public void TestEmpContraIndexCallAllMethodFromRepository()//este metodo no utilizo en el index
        {
            var mock = new Mock<InterfaceEmpContratista>();

            var query = "";

            mock.Setup(o => o.ByQueryAll(query)).Returns(new List<EmpContratista>());

            var controller = new EmpContratistaController(mock.Object, null);

            controller.Index();

            mock.Verify(o => o.AllEmpContratista(), Times.Exactly(0));
        }

        //----------------------------------------------------------------------------------------
        [Test]
        public void TestEmpContraCreateReturnView()
        {
            var controller = new EmpContratistaController(null,null);

            var view = controller.Create();

            Assert.IsInstanceOf(typeof(ViewResult), view);
        }

        [Test]
        public void TestEmpContraPostCreateOKReturnRedirect()
        {
            var empContratista = new EmpContratista();

            var repositoryMock = new Mock<InterfaceEmpContratista>();

            repositoryMock.Setup(o => o.AddEmpContratista(new EmpContratista()));

            var validatorMock = new Mock<EmpContratistaValidator>();

            validatorMock.Setup(o => o.Pass(empContratista)).Returns(true);

            var controller = new EmpContratistaController(repositoryMock.Object, validatorMock.Object);

            var redirect = (RedirectToRouteResult)controller.Create(empContratista);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestEmpContraPostCreateCallAddMethodFromRepository()
        {
            var empContratista = new EmpContratista();

            var repositoryMock = new Mock<InterfaceEmpContratista>();

            repositoryMock.Setup(o => o.AddEmpContratista(new EmpContratista()));

            var validatorMock = new Mock<EmpContratistaValidator>();

            validatorMock.Setup(o => o.Pass(empContratista)).Returns(true);

            var controller = new EmpContratistaController(repositoryMock.Object, validatorMock.Object);

            var redirect = controller.Create(empContratista);

            repositoryMock.Verify(o => o.AddEmpContratista(empContratista), Times.Once());

        }

        [Test]

        public void TestEmpContraPostCreateReturnViewWithErrorsWhenValidationFail()
        {
            var empContratista = new EmpContratista();

            var repositoryMock = new Mock<InterfaceEmpContratista>();

            repositoryMock.Setup(o => o.AddEmpContratista(new EmpContratista()));

            var validatorMock = new Mock<EmpContratistaValidator>();

            validatorMock.Setup(o => o.Pass(empContratista)).Returns(true);

            var controller = new EmpContratistaController(repositoryMock.Object, validatorMock.Object);

            var view = controller.Create(empContratista);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), view);
        }
        //----------------------------------------------------------------------------------------

        [Test]
        public void TestEmpContraEditReturnView()
        {
            var empContratista = new EmpContratista();

            var repositoryMock = new Mock<InterfaceEmpContratista>();

            repositoryMock.Setup(o => o.FindEmpContratista(empContratista.Id)).Returns(new EmpContratista());

            var validatorMock = new Mock<EmpContratistaValidator>();

            validatorMock.Setup(o => o.Pass(empContratista)).Returns(true);

            var controller = new EmpContratistaController(repositoryMock.Object, validatorMock.Object);
            var view = controller.Edit(empContratista.Id);

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Edit", view.ViewName);
        }

        [Test]
        public void TestEmpContraPostEditOKReturnRedirect()
        {
            var empContratista = new EmpContratista();

            var repositoryMock = new Mock<InterfaceEmpContratista>();

            repositoryMock.Setup(o => o.FindEmpContratista(empContratista.Id)).Returns(new EmpContratista());

            var validatorMock = new Mock<EmpContratistaValidator>();

            validatorMock.Setup(o => o.Pass(empContratista)).Returns(true);

            var controller = new EmpContratistaController(repositoryMock.Object, validatorMock.Object);

            var redirect = (RedirectToRouteResult)controller.Edit(empContratista);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestEmpContraPostEdirCallUpdateMethodFromRepository()
        {
            var empContratista = new EmpContratista();

            var repositoryMock = new Mock<InterfaceEmpContratista>();

            repositoryMock.Setup(o => o.FindEmpContratista(empContratista.Id)).Returns(new EmpContratista());

            var validatorMock = new Mock<EmpContratistaValidator>();

            validatorMock.Setup(o => o.Pass(empContratista)).Returns(true);

            var controller = new EmpContratistaController(repositoryMock.Object, validatorMock.Object);

            var redirect = controller.Edit(empContratista);

            repositoryMock.Verify(o => o.UpdateEmpContratista(empContratista), Times.Once());

        }

        [Test]

        public void TestEmpContraPostEditReturnViewWithErrorsWhenValidationFail()
        {
            var empContratista = new EmpContratista();

            var repositoryMock = new Mock<InterfaceEmpContratista>();

            repositoryMock.Setup(o => o.FindEmpContratista(empContratista.Id)).Returns(new EmpContratista());

            var validatorMock = new Mock<EmpContratistaValidator>();

            validatorMock.Setup(o => o.Pass(empContratista)).Returns(true);

            var controller = new EmpContratistaController(repositoryMock.Object, validatorMock.Object);

            var view = controller.Edit(empContratista);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), view);
        }
        //----------------------------------------------------------------------------------------

        [Test]
        public void TestEmpContraDeleteReturnRouteResult()
        {
            var empContratista = new EmpContratista();
            var repositoryMock = new Mock<InterfaceEmpContratista>();

            var controller = new EmpContratistaController(repositoryMock.Object, null);

            var view = controller.Delete(empContratista.Id);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), view);
        }

        [Test]
        public void TestEmpContraDetailsReturnView()
        {
            var empContratista = new EmpContratista();
            var repositoryMock = new Mock<InterfaceEmpContratista>();

            var controller = new EmpContratistaController(repositoryMock.Object, null);

            var view = controller.Details(empContratista.Id);

            Assert.AreEqual("Details", view.ViewName);
        }
    }
}
